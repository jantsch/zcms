-- COPYRIGHT 2015 (C) Gustavo Jantsch <jantsch@gmail.com>
-- POSTGRES VERSION
-- Table: public."user"
-- password : sha1 40 char hash


-- ======== BANNER

DROP TABLE IF EXISTS public.banner;
DROP SEQUENCE IF EXISTS public.banner_id_seq;
CREATE SEQUENCE banner_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE banner_id_seq OWNER TO postgres;

CREATE TABLE public.banner
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('banner_id_seq'::regclass),
  title character varying(255) NOT NULL,
  header character varying(255),
  href character varying(255),
  file character varying(255),
  status gallery_status DEFAULT 'public',
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer,
  disabled_by integer
)
WITH (OIDS=FALSE);
ALTER TABLE banner OWNER TO postgres;


-- ========== CONFIG

CREATE TABLE public.config
(
  property character varying(255) NOT NULL,
  value character varying(255) NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE public."config" OWNER TO postgres;

-- ========== USER

DROP TABLE IF EXISTS public.user;
DROP SEQUENCE IF EXISTS public.user_id_seq;

CREATE SEQUENCE public.user_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.user_id_seq
  OWNER TO postgres;

CREATE TABLE public.user
(
  id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
  first_name character varying(50) NOT NULL,
  last_name character varying(50),
  email character varying(255) NOT NULL,
  phone character varying(50),
  password character(40) NOT NULL,
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer,
  disabled_by integer,
  is_admin bool DEFAULT false,
  last_login timestamp without time zone,
  CONSTRAINT id PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
);
ALTER TABLE public."user" OWNER TO postgres;

-- ======= GALERIA

DROP TABLE IF EXISTS public.gallery;
DROP TABLE IF EXISTS public.gallery_image;
DROP SEQUENCE IF EXISTS public.gallery_id_seq;
DROP SEQUENCE IF EXISTS public.gallery_image_id_seq;
DROP TYPE IF EXISTS gallery_status;

CREATE SEQUENCE public.gallery_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.gallery_id_seq OWNER TO postgres;

CREATE SEQUENCE public.gallery_image_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.gallery_image_id_seq OWNER TO postgres;

CREATE TYPE gallery_status AS ENUM ('public', 'private');

CREATE TABLE public.gallery
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('gallery_id_seq'::regclass),
  title character varying(255) NOT NULL,
  description text,
  status gallery_status DEFAULT 'private',
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer,
  disabled_by integer
)
WITH (OIDS=FALSE);
ALTER TABLE public."gallery" OWNER TO postgres;

CREATE TABLE public.gallery_image
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('gallery_image_id_seq'::regclass),
  gallery_id integer,
  title character varying(255) NOT NULL,
  description text,
  file character varying(255) NOT NULL,
  order integer DEFAULT 0,
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer,
  disabled_by integer
)
WITH (OIDS=FALSE);
ALTER TABLE public."gallery_image" OWNER TO postgres;

-- ======= PAGE
DROP TABLE IF EXISTS public.page;
DROP SEQUENCE IF EXISTS public.page_id_seq;

CREATE SEQUENCE public.page_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE public.page_id_seq
OWNER TO postgres;

CREATE TABLE public.page
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('page_id_seq'::regclass),
  title character varying(255) NOT NULL,
  status doc_status DEFAULT 'revision',
  content text,
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer
)
WITH (
OIDS=FALSE
);
ALTER TABLE public."page" OWNER TO postgres;


-- ======= POST
DROP TABLE IF EXISTS public.post;
DROP SEQUENCE IF EXISTS public.post_id_seq;

CREATE SEQUENCE public.post_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.post_id_seq OWNER TO postgres;

DROP TYPE IF EXISTS doc_status;
CREATE TYPE doc_status AS ENUM ('revision', 'draft', 'published');

CREATE TABLE public.post
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('post_id_seq'::regclass),
  title character varying(255) NOT NULL,
  status doc_status DEFAULT 'revision',
  description text,
  content text,
  image varchar(255),
  published_on timestamp default now(),
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer
)
WITH (OIDS=FALSE);
ALTER TABLE public."post" OWNER TO postgres;

-- POST CATEGORIES

DROP SEQUENCE IF EXISTS public.categ_id_seq;
DROP INDEX IF EXISTS idx_post_categories;
DROP TABLE IF EXISTS public.categories;

CREATE SEQUENCE public.categ_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.categ_id_seq OWNER TO postgres;

CREATE TABLE public.category
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('categ_id_seq'::regclass),
  title character varying(255) NOT NULL,
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer
)
WITH (OIDS=FALSE);
ALTER TABLE public."post" OWNER TO postgres;

CREATE TABLE public.post_categories (
  post_id integer NOT NULL,
  category_id integer NOT NULL
)WITH (OIDS=FALSE);
ALTER TABLE public."post" OWNER TO postgres;
CREATE UNIQUE INDEX idx_post_categories ON post_categories (post_id, category_id);

-- ======= SLUG
DROP INDEX IF EXISTS slug_idx_post_id;
DROP INDEX IF EXISTS slug_idx_page_id;
DROP INDEX IF EXISTS slug_idx_prod_id;
DROP TABLE IF EXISTS public.slug;
DROP SEQUENCE IF EXISTS public.slug_id_seq;

CREATE SEQUENCE public.slug_id_seq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
ALTER TABLE public.slug_id_seq
OWNER TO postgres;

CREATE TABLE public.slug
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('slug_id_seq'::regclass),
  slug character varying(255) NOT NULL,
  post_id integer DEFAULT NULL,
  page_id integer DEFAULT NULL,
  product_id integer DEFAULT NULL,
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer

)
WITH (
OIDS=FALSE
);
ALTER TABLE public."post" OWNER TO postgres;

CREATE INDEX slug_idx_post_id ON public.slug (post_id);
CREATE INDEX slug_idx_page_id ON public.slug (page_id);
CREATE INDEX slug_idx_prod_id ON public.slug (product_id);


-- ======= PRODUCT
DROP TABLE IF EXISTS public.product;
DROP SEQUENCE IF EXISTS public.prod_id_seq;
DROP TYPE IF EXISTS public.enabled_disabled_status;
DROP TABLE IF EXISTS public.product_image;
DROP SEQUENCE IF EXISTS public.product_image_id_seq;
DROP TABLE IF EXISTS public.procat;
DROP SEQUENCE IF EXISTS public.procat_id_seq;
DROP TABLE IF EXISTS public.product_procat;
DROP INDEX IF EXISTS public.idx_product_procat;
DROP TABLE IF EXISTS public.product_related;
DROP INDEX IF EXISTS public.idx_product_related;

CREATE SEQUENCE public.product_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.product_id_seq OWNER TO postgres;

CREATE TYPE enabled_disabled_status AS ENUM ('enabled', 'disabled');

CREATE TABLE public.product
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('product_id_seq'::regclass),
  name character varying(255) NOT NULL,
  status enabled_disabled_status DEFAULT 'disabled',
  description text,
  info text,
  tips text,
  main_image_id integer,
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer
) WITH (OIDS=FALSE);
ALTER TABLE public."product" OWNER TO postgres;

CREATE SEQUENCE public.product_image_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.product_image_id_seq OWNER TO postgres;

CREATE TABLE public.product_image
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('product_image_id_seq'::regclass),
  product_id integer NOT NULL,
  file CHARACTER VARYING(255),
  description CHARACTER VARYING(255),
  created_on timestamp without time zone DEFAULT now(),
  created_by integer
) WITH (OIDS=FALSE);
ALTER TABLE public."product" OWNER TO postgres;

CREATE SEQUENCE public.procat_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;
ALTER TABLE public.procat_id_seq OWNER TO postgres;

CREATE TABLE public.procat
(
  id integer NOT NULL PRIMARY KEY DEFAULT nextval('procat_id_seq'::regclass),
  title CHARACTER VARYING(255),
  created_on timestamp without time zone DEFAULT now(),
  modified_on timestamp without time zone,
  disabled_on timestamp without time zone,
  created_by integer,
  modified_by integer
) WITH (OIDS=FALSE);
ALTER TABLE public."product" OWNER TO postgres;


CREATE TABLE public.product_procat
(
  product_id INTEGER NOT NULL,
  procat_id INTEGER NOT NULL,
  created_on timestamp without time zone DEFAULT now(),
  created_by integer
) WITH (OIDS=FALSE);
ALTER TABLE public."product_procat" OWNER TO postgres;
CREATE UNIQUE INDEX idx_product_procat ON product_procat (product_id, procat_id);

CREATE TABLE public.product_related
(
  product_id INTEGER NOT NULL,
  related_id INTEGER NOT NULL,
  created_on timestamp without time zone DEFAULT now(),
  created_by integer
) WITH (OIDS=FALSE);
ALTER TABLE public."product_procat" OWNER TO postgres;
CREATE UNIQUE INDEX idx_product_related ON product_related (product_id, related_id);
