-- COPYRIGHT 2015 (C) Gustavo Jantsch <jantsch@gmail.com>
-- NOTES
--     user.password : sha1 40 char hash

SET DEFAULT_STORAGE_ENGINE = InnoDB;

-- ======== BANNER
DROP TABLE IF EXISTS banner;

CREATE TABLE banner
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  header VARCHAR(255),
  href VARCHAR(255),
  file VARCHAR(255),
  status ENUM('public', 'private') DEFAULT 'public',
  posicao enum('normal', 'topo') DEFAULT 'normal',
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED,
  disabled_by INTEGER UNSIGNED
);


-- ========== CONFIG

DROP TABLE IF EXISTS config;

CREATE TABLE config
(
  property VARCHAR(255) NOT NULL,
  value VARCHAR(255) NOT NULL
);

-- ========== USER

DROP TABLE IF EXISTS user;

CREATE TABLE user
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50),
  email VARCHAR(255) NOT NULL,
  phone VARCHAR(50),
  password character(40) NOT NULL,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED,
  disabled_by INTEGER UNSIGNED,
  is_admin BOOLEAN DEFAULT false,
  last_login DATETIME,
  CONSTRAINT id PRIMARY KEY (id)
);

INSERT INTO user (first_name, email, password, is_admin) VALUES ('Administrador', 'x@x.com', SHA1('123'), TRUE);

-- ======= GALLERY

DROP TABLE IF EXISTS gallery;
DROP TABLE IF EXISTS gallery_image;

CREATE TABLE gallery
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  description MEDIUMTEXT,
  status ENUM('public', 'private'),
  image_id INTEGER UNSIGNED,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED,
  disabled_by INTEGER UNSIGNED
);


CREATE TABLE gallery_image
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  gallery_id INTEGER UNSIGNED,
  title VARCHAR(255) NOT NULL,
  description MEDIUMTEXT,
  file VARCHAR(255) NOT NULL,
  order INTEGER UNSIGNED DEFAULT 0,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED,
  disabled_by INTEGER UNSIGNED
);

-- ======= PAGE

DROP TABLE IF EXISTS page;

CREATE TABLE page
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  status ENUM('draft', 'revision', 'published'),
  content LONGTEXT,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED
);

-- ======= POST

DROP TABLE IF EXISTS post;

CREATE TABLE post
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  status ENUM ('revision', 'draft', 'published'),
  description LONGTEXT,
  content LONGTEXT,
  image VARCHAR(255),
  published_on DATETIME DEFAULT now(),
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED
);

-- POST CATEGORIES

DROP TABLE IF EXISTS category;

CREATE TABLE category
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED
);

DROP TABLE IF EXISTS post_categories;

CREATE TABLE post_categories (
  post_id INTEGER UNSIGNED NOT NULL,
  category_id INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY idx_post_categories (post_id, category_id)
);


-- ======= SLUG

DROP TABLE IF EXISTS slug;

CREATE TABLE slug
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  slug VARCHAR(255) NOT NULL,
  post_id INTEGER UNSIGNED DEFAULT NULL,
  page_id INTEGER UNSIGNED DEFAULT NULL,
  product_id INTEGER UNSIGNED DEFAULT NULL,
  product_type_id INTEGER UNSIGNED,
  category_id INTEGER UNSIGNED,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED,
  INDEX slug_idx_post_id (post_id),
  INDEX slug_idx_page_id (page_id),
  INDEX slug_idx_prod_id (product_id),
  INDEX slug_idx_prod_type_id (product_type_id),
  INDEX slug_idx_category_id (category_id)
);

-- ======= PRODUCT

DROP TABLE IF EXISTS product;
DROP TABLE IF EXISTS product_image;
DROP TABLE IF EXISTS product_type;
DROP TABLE IF EXISTS procat;
DROP TABLE IF EXISTS product_procat;
DROP TABLE IF EXISTS product_related;

CREATE TABLE product
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  type_id INTEGER UNSIGNED NOT NULL,
  status ENUM ('enabled', 'disabled') DEFAULT 'disabled',
  description LONGTEXT,
  info LONGTEXT,
  tips LONGTEXT,
  main_image_id INTEGER UNSIGNED,
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED
);

CREATE TABLE product_type
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title CHARACTER VARYING(255),
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED
);

CREATE TABLE product_image
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  product_id INTEGER UNSIGNED NOT NULL,
  file CHARACTER VARYING(255),
  description CHARACTER VARYING(255),
  created_on DATETIME DEFAULT now(),
  created_by INTEGER UNSIGNED
);

CREATE TABLE procat
(
  id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title CHARACTER VARYING(255),
  created_on DATETIME DEFAULT now(),
  modified_on DATETIME,
  disabled_on DATETIME,
  created_by INTEGER UNSIGNED,
  modified_by INTEGER UNSIGNED
);

CREATE TABLE product_procat
(
  product_id INTEGER UNSIGNED NOT NULL,
  procat_id INTEGER UNSIGNED NOT NULL,
  created_on DATETIME DEFAULT now(),
  created_by INTEGER UNSIGNED,
  PRIMARY KEY idx_product_procat (product_id, procat_id)
);


CREATE TABLE product_related
(
  product_id INTEGER UNSIGNED NOT NULL,
  related_id INTEGER UNSIGNED NOT NULL,
  created_on DATETIME DEFAULT now(),
  created_by INTEGER UNSIGNED,
  PRIMARY KEY idx_product_related (product_id, related_id)
);


CREATE TABLE post_comment
(
  id INTEGER UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(100),
  email VARCHAR(100),
  subject VARCHAR(255),
  message MEDIUMTEXT,
  status ENUM('review', 'approved', 'rejected'),
  user_id INTEGER UNSIGNED,
  post_id INTEGER UNSIGNED,
  created_on DATETIME,
  reply_to INTEGER UNSIGNED,
  INDEX idx_post_comment_status (status),
  INDEX idx_post_comment_time (post_id, status, created_on)
);

DROP TABLE IF EXISTS tag;

CREATE TABLE tag
(
  tag VARCHAR(25) PRIMARY KEY,
  created_on DATETIME DEFAULT now(),
  created_by INTEGER UNSIGNED
);

DROP TABLE IF EXISTS post_tag;

CREATE TABLE post_tag
(
  post_id INTEGER UNSIGNED,
  tag VARCHAR(25),
  PRIMARY KEY idx_post_tag_unique (post_id, tag),
  INDEX idx_post_tag_post_id (post_id)
);
