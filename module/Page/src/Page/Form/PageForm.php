<?php
/**
 * PageForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Page\Form;

use Zend\Form\Form;

class PageForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('page');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'slug',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    'revision' => 'revisao',
                    'draft' => 'rascunho',
                    'published' => 'publicado'
                )
            )
        ));

        $this->add(array(
            'name' => 'content',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}