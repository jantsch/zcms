<?php
/**
 * PageTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Page\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;
use Slug\Model\Slug;
use Slug\Model\SlugTable;
use Zend\Db\Sql\Where;

class PageTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $slugTable = null;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function getSlugTable() {

        if($this->slugTable==null ) {
            $this->slugTable = $this->serviceLocator->get('Slug\Model\SlugTable');
        }
        return $this->slugTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPage($id)
    {
        $id  = (int) $id;


        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('*'));
        $sqlSelect->join('slug', 'slug.page_id = page.id', array('slug'), 'left');

        $w = new Where();
        $w->equalTo('page.id', $id);
        $sqlSelect->where($w);

        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($sqlSelect);

        $resultSet = $statement->execute();
        $row = $resultSet->current();
        $page = new Page();
        $page->exchangeArray($row);

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $page;
    }

    public function getPageBySlug($slug)
    {
        $slug = filter_var($slug, FILTER_SANITIZE_STRING);


        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('*'));
        $sqlSelect->join('slug', 'slug.page_id = page.id', array('slug'));

        $w = new Where();
        $w->equalTo('page.status', 'published');
        $w->equalTo('slug.slug', $slug);
        $sqlSelect->where($w);

        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($sqlSelect);

        $resultSet = $statement->execute();
        $row = $resultSet->current();
        $page = new Page();
        $page->exchangeArray($row);

        if (!$row) {
//            throw new \Exception("Could not find page with slug $slug");
        }
        return $page;
    }

    public function savePage(Page $page)
    {
        $data = array(
            'content' => $page->content,
            'title'  => $page->title,
            'status' => $page->status,
        );

        $id = (int) $page->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $id = (int)$this->tableGateway->getLastInsertValue();

        } else {
            if ($this->getPage($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Page id does not exist');
            }
        }

        $tslug = $this->getSlugTable();
        $slug = $tslug->getSlugFor('page', $id);

        if(is_null($slug)) {
            $slug = new Slug();
        }

        $slug->slug = $page->slug;
        $slug->page_id = $id;
        $tslug->saveSlug($slug);

    }

    public function deletePage($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));

        $tslug = $this->getSlugTable();
        $tslug->deleteSlugFor('page', $id);
    }
}