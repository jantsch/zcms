<?php
/**
 * PageController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Page\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Page\Model\Page;
use Page\Form\PageForm;
use Login\Controller\LoginController;


class PageController extends AbstractActionController
{
    protected $pageTable;

    public function indexAction()
    {
        LoginController::checkAuthentication();

        // changes the layout for this specific action
        // $this->layout('layout/page');

        return new ViewModel(array(
            'pages' => $this->getPageTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new PageForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $page = new Page();
            $page->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
            $form->setInputFilter($page->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $page->exchangeArray($form->getData());
                $this->getPageTable()->savePage($page);

                // Redirect to list of pages
                return $this->redirect()->toRoute('page');
            }
        }
        return array('form' => $form);
    }

    public function editAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('page', array(
                'action' => 'add'
            ));
        }

        try {
            $page = $this->getPageTable()->getPage($id);
            $page->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('page', array(
                'action' => 'index'
            ));
        }

        $form  = new PageForm();
        $form->bind($page);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($page->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getPageTable()->savePage($page);

                // Redirect to list of pages
                return $this->redirect()->toRoute('page');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('page');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {
                $id = (int) $request->getPost('id');
                $this->getPageTable()->deletePage($id);
            }

            // Redirect to list of pages
            return $this->redirect()->toRoute('page');
        }

        return array(
            'id'    => $id,
            'page' => $this->getPageTable()->getPage($id)
        );
    }

    public function getPageTable()
    {
        if (!$this->pageTable) {
            $sm = $this->getServiceLocator();
            $this->pageTable = $sm->get('Page\Model\PageTable');
        }
        return $this->pageTable;
    }
}