<?php
/**
 * UserForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace User\Form;

use Zend\Form\Form;

class UserForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('user');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'first_name',
            'type' => 'Text'
        ));
        $this->add(array(
            'name' => 'last_name',
            'type' => 'Text'
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'Text'
        ));
         $this->add(array(
            'name' => 'phone',
            'type' => 'Text'
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'Password'
        ));

         $this->add(array(
            'name' => 'password_confirm',
            'type' => 'Password'
        ));

         $this->add(array(
            'name' => 'is_admin',
            'type' => 'Checkbox'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}