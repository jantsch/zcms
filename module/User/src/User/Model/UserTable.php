<?php
/**
 * UserTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace User\Model;

use Login\Controller\LoginController;
use Zend\Db\TableGateway\TableGateway;
use Zend\Form\Exception\ExtensionNotLoadedException;

class UserTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select){
            $select->order('first_name');
        });
        return $resultSet;
    }

    public function getUser($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Authenticate user on user's table and update last login.
     *
     * @author Gustavo Jantsch
     * @param string $email
     * @param string $password
     * @return array|\ArrayObject|null
     * @throws \Exception
     */
    public function authenticateUser($email, $password)
    {
        $email = filter_var($email, FILTER_SANITIZE_STRING);
        $password = sha1($password);
        $rowset = $this->tableGateway->select(array('email' => $email), array('password'=>$password));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Email ou senha n�o conferem.");
        }

        // update last login
        $id = intval($row->id);
        if($id>0) {
            $this->getUser($id);
            $this->tableGateway->update(array('last_login' => date('Y-m-d H:i:s')), array('id' => $id));
        }

        return $row;
    }

    public function saveUser(User $user)
    {
        $data = array(
            'first_name' => $user->first_name,
            'last_name'  => $user->last_name,
            'email'  => $user->email,
            'phone'  => $user->phone,
            'is_admin' => $user->is_admin
        );

        if(!empty($user->password)) {

            $data['password']  = sha1($user->password);
        }

        $id = (int) $user->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('User id does not exist');
            }
        }
    }

    public function deleteUser($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}