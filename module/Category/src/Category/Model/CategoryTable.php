<?php
/**
 * CategoryTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Category\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Where;
use Login\Controller\LoginController;
use Slug\Model\Slug;

class CategoryTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $slugTable = null;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function getSlugTable() {

        if($this->slugTable==null ) {
            $this->slugTable = $this->serviceLocator->get('Slug\Model\SlugTable');
        }
        return $this->slugTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select){
            $select->join('slug', 'slug.category_id = category.id', array('slug'), 'left');
            $select->order('title ASC');
        });

        return $resultSet;
    }

    public function getCategory($id)
    {
        $id  = (int) $id;
        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('*'));
        $sqlSelect->join('slug', 'slug.category_id = category.id', array('slug'), 'left');

        $w = new Where();
        $w->equalTo('category.id', $id);
        $sqlSelect->where($w);

        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($sqlSelect);

        $resultSet = $statement->execute();
        $row = $resultSet->current();
        $category = new Category();
        $category->exchangeArray($row);
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $category;
    }

    public function saveCategory(Category $category)
    {
        $data = array(
            'title'  => $category->title,
        );

        $id = (int) $category->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $id = $this->tableGateway->getLastInsertValue();
            // $id =  $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('categ_id_seq');

        } else {
            if ($this->getCategory($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Pae id does not exist');
            }
        }

        $tslug = $this->getSlugTable();
        $slug = $tslug->getSlugFor('category', $id);

        if(is_null($slug)) {
            $slug = new Slug();
        }

        $slug->slug = $category->slug;
        $slug->category_id = $id;
        $tslug->saveSlug($slug);


    }

    public function deleteCategory($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
        $tslug = $this->getSlugTable();
        $tslug->deleteSlugFor('category', $id);

    }
}