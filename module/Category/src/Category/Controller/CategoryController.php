<?php
/**
 * CategoryController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Category\Controller;

use Category\Form\CategoryForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Category\Model\Category;
use Login\Controller\LoginController;

class CategoryController extends AbstractActionController
{
    protected $categoryTable;
    protected $config;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }
        return $this->config;
    }

    public function indexAction()
    {

        LoginController::checkAuthentication();

        return new ViewModel(array(
            'categories' => $this->getCategoryTable()->fetchAll(),
        ));
    }
    
    public function saveAction()
    {

        LoginController::checkAuthentication();

        $request = $this->getRequest();
        $title = $request->getPost('title', null);
        $json = array('msg' => 'Categoria inválida', 'id' => null, 'title' => null);
        if (!empty($title)) {

            $tbl = $this->getCategoryTable();
            $category = $tbl->getCategory($title);
            $exist = ($category ? $category->getCategory($title) : false);

            if($exist) {
                $json = array('msg' => 'Categoria já existe.', 'id' => $exist->id, 'title' => $exist->title);

            }else {
                $category = new Category();
                $category->title = $title;
                $id = $this->getCategoryTable()->saveCategory($category);
                $json = array('msg' => 'Categoria criada.', 'id' => $id, 'title' => $title);
            }
        }

        $this->layout('layout/json');
        return array('data'=> $json);

    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $request = $this->getRequest();
        $id = (int)$this->params()->fromRoute('id', 0);

        // ajax request
        if($request->getPost('isAjax', 0)) {
            $json = array('msg' => 'Id inválido.');
            if ($id) {
                if ($request->isCategory()) {
                    $id = (int)$request->getCategory('id');
                    $this->getCategoryTable()->deleteCategory($id);

                    $json = array('msg' => 'Categoria removida.');
                }
            }

            $this->layout('site/json');
            return array('data' => $json);
        }


        // normal request
        if (!$id) {
            return $this->redirect()->toRoute('category');
        }

        $postTable = $this->getPostTable();
        if($postTable->getPostsByCategory($id)) {
            die('nao dá pra apagar');
        }


        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');
            if ($del == 'Remover') {
                $this->getCategoryTable()->deleteCategory($id);
            }
            // Redirect to list of categorys
            return $this->redirect()->toRoute('category');
        }

        return array(
            'id'    => $id,
            'category' => $this->getCategoryTable()->getCategory($id)
        );

    }

    public function handleUpload($files, $id)
    {
        $config = $this->getConfig();
        $name = null;

        if(isset($files['file']) && $files['file']['error']==UPLOAD_ERR_OK) {
            $file = $files['file'];

            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($file['name']);
            if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

                if (!is_dir($config['category_upload_folder'])) {
                    mkdir($config['category_upload_folder']);
                    chmod($config['category_upload_folder'], 0775);
                }

                $name = $id . ".jpg";
                $origin = $file['tmp_name'];
                $target = $config['category_upload_folder'] . $name;

                $w = \WideImage::load($origin);
                $w = $w->resize(278, 298, 'outside');
                $w = $w->crop('center', 'center', 278, 298);
                $w->saveToFile($target);

            }
        }

        return $name;
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new CategoryForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $category = new Category();
            $category->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
            $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $category->exchangeArray($form->getData());
                $id = $this->getCategoryTable()->saveCategory($category);
                $this->handleUpload($request->getFiles()->toArray(), $id);

                // Redirect to list of categories
                return $this->redirect()->toRoute('category');
            }
        }
        return array(
            'form' => $form,
            'config' => $this->getConfig()
        );
    }

    public function editAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('category', array(
                'action' => 'add'
            ));
        }

        try {
            $category = $this->getCategoryTable()->getCategory($id);
            $category->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('category', array(
                'action' => 'index'
            ));
        }

        $form  = new CategoryForm();
        $form->bind($category);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($category->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getCategoryTable()->saveCategory($category);
                $this->handleUpload($request->getFiles()->toArray(), $id);

                return $this->redirect()->toRoute('category');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
            'config' => $this->getConfig()
        );
    }


    public function getCategoryTable()
    {
        if (!$this->categoryTable) {
            $sm = $this->getServiceLocator();
            $this->categoryTable = $sm->get('Category\Model\CategoryTable');
        }
        return $this->categoryTable;
    }

    protected $postTable;

    public function getPostTable()
    {
        if (!$this->postTable) {
            $sm = $this->getServiceLocator();
            $this->postTable = $sm->get('Post\Model\PostTable');
        }
        return $this->postTable;
    }
}