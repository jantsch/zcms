<?php
/**
 * Slug.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Slug\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Slug implements InputFilterAwareInterface
{
    public $id;
    public $slug;
    public $post_id;
    public $page_id;
    public $product_id;
    public $product_type_id;
    public $category_id;
    public $created_on;
    public $modified_on;
    public $created_by;
    public $modified_by;
    public $disabled_by;

    protected $inputFilter;
    protected $dbAdapter;

    public function setDbAdapter($adapter){

        $this->dbAdapter = $adapter;
    }

    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->slug  = (!empty($data['slug'])) ? $data['slug'] : null;
        $this->post_id = (!empty($data['post_id'])) ? $data['post_id'] : null;
        $this->page_id  = (!empty($data['page_id'])) ? $data['page_id'] : null;
        $this->product_id  = (!empty($data['product_id'])) ? $data['product_id'] : null;
        $this->product_type_id  = (!empty($data['product_type_id'])) ? $data['product_type_id'] : null;
        $this->category_id  = (!empty($data['category_id'])) ? $data['category_id'] : null;
        $this->created_on  = (!empty($data['created_on'])) ? $data['created_on'] : null;
        $this->created_by  = (!empty($data['created_by'])) ? $data['created_by'] : null;
        $this->modified_on  = (!empty($data['modified_on'])) ? $data['modified_on'] : null;
        $this->modified_by  = (!empty($data['modified_by'])) ? $data['modified_by'] : null;
        $this->disabled_by  = (!empty($data['disabled_by'])) ? $data['disabled_by'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'page_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'post_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'product_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'slug',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                )
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
