<?php
/**
 * SlugTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Slug\Model;

use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;

class SlugTable
{
    protected $tableGateway;
    protected $types = array('post_id', 'page_id', 'product_id', 'product_type_id', 'category_id');

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getSlug($id)
    {
        if( (int)$id >0) {
            $id=(int)$id;
            $rowset = $this->tableGateway->select(array('id' => $id));

        }elseif(!empty($id)) {
            $rowset = $this->tableGateway->select(array('slug' => $id));

        }else{
            return null;
        }

        $row = $rowset->current();
        return $row;
    }

    /**
     * @param $type post/page/product
     * @param $id
     * @return array|\ArrayObject|null
     * @throws \Exception
     */
    public function getSlugFor($type, $id)
    {

        $type = filter_var($type, FILTER_SANITIZE_STRING) . "_id";

        if(!in_array($type, $this->types)) {
            return null;
        }

        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array($type => $id));
        $row = $rowset->current();
        if (!$row) {
            //throw new \Exception("Could not find slug $id for $type");
            return null;
        }
        return $row;
    }

    public function saveSlug(Slug $slug)
    {
        $data = array(
            'slug' => $slug->slug,
            'page_id' => $slug->page_id,
            'post_id' => $slug->post_id,
            'product_id' => $slug->product_id,
            'product_type_id' => $slug->product_type_id,
            'category_id' => $slug->category_id
        );

        $id = (int) $slug->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
        } else {
            if ($this->getSlug($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Slug id does not exist');
            }
        }
    }

    public function deleteSlugFor($type, $id) {
        $type = $type . '_id';
        if(in_array($type, $this->types)){
            $id = (int)$id;
            $this->tableGateway->delete(array($type => (int) $id));
        }
    }

    public function deleteSlug($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}