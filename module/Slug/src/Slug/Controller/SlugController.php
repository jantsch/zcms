<?php
/**
 * SlugController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Slug\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Slug\Model\Slug;

class SlugController extends AbstractActionController
{
    protected $slugTable;

    public function getSlugTable()
    {
        if (!$this->slugTable) {
            $sm = $this->getServiceLocator();
            $this->slugTable = $sm->get('Slug\Model\SlugTable');
        }
        return $this->slugTable;
    }
}