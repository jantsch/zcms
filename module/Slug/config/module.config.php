<?php
/**
 * module.config.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Slug\Controller\Slug' => 'Slug\Controller\SlugController',
        ),
    )
);