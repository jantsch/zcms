<?php
/**
 * ProductController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Product\Model\ProductType;
use Product\Form\ProductTypeForm;
use Login\Controller\LoginController;

class ProductTypeController extends AbstractActionController
{
    protected $protypeTypeTable;
    protected $config;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }

        return $this->config;

    }

    public function getProductTypeTable()
    {
        if (!$this->protypeTypeTable) {
            $sm = $this->getServiceLocator();
            $this->protypeTypeTable = $sm->get('Product\Model\ProductTypeTable');
        }
        return $this->protypeTypeTable;
    }

    public function handleUpload($files, $id)
    {
        $config = $this->getConfig();
        $name = null;
        // handle upload
        if(isset($files['file']) && $files['file']['error']==UPLOAD_ERR_OK) {
            $file = $files['file'];
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($file['name']);
            if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

                if(!is_dir($config['protype_upload_folder'])) {
                    mkdir($config['protype_upload_folder']);
                    chmod($config['protype_upload_folder'], 0775);
                }
                $name = $id . ".jpg";
                $origin = $file['tmp_name'];
                $target = $config['protype_upload_folder'] . $name;

                $w = \WideImage::load($origin);
                $w->saveToFile($target);

            }
        }

        if(isset($files['logo']) && $files['logo']['error']==UPLOAD_ERR_OK) {
            $file = $files['logo'];
            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($file['name']);
            if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

                if(!is_dir($config['protype_upload_folder'] . 'logo/')) {
                    mkdir($config['protype_upload_folder'] . 'logo/');
                    chmod($config['protype_upload_folder'] . 'logo/', 0775);
                }
                $name = $id . ".jpg";
                $origin = $file['tmp_name'];
                $target = $config['protype_upload_folder'] . 'logo/' . $name;

                $w = \WideImage::load($origin);
                $w->saveToFile($target);

            }
        }

        return $name;
    }

    public function indexAction()
    {
        LoginController::checkAuthentication();

        return new ViewModel(array(
            'protypes' => $this->getProductTypeTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new ProductTypeForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $protype = new ProductType();
            $protype->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
            $form->setInputFilter($protype->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $protype->exchangeArray($form->getData());
                $id = $this->getProductTypeTable()->saveProductType($protype);
                $this->handleUpload($request->getFiles()->toArray(), $id);

                return $this->redirect()->toRoute('protype');
            }
        }
        return array(
            'form' => $form,
            'config' => $this->getConfig()
        );
    }

    public function editAction()
    {
        LoginController::checkAuthentication();


        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('protype', array('action' => 'add'));
        }

        try {
            $protype = $this->getProductTypeTable()->getProductType($id);
            $protype->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));

        }
        catch (\Exception $ex) {

            return $this->redirect()->toRoute('protype', array('action' => 'index'));
        }

        $form  = new ProductTypeForm();
        $form->bind($protype);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($protype->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getProductTypeTable()->saveProductType($protype);
                $name= $this->handleUpload($request->getFiles()->toArray(), $id);
                if (!is_null($name)) {
                    return $this->redirect()->toRoute('protype', array('action' => 'edit', 'id' => $id));
                }
                return $this->redirect()->toRoute('protype', array('action' => 'index'));
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
            'config' => $this->getConfig()
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('protype');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {
                $id = (int) $request->getPost('id');
                $this->getProductTypeTable()->deleteProductType($id);
            }

            // Redirect to list of protypes
            return $this->redirect()->toRoute('protype');
        }

        return array(
            'id'    => $id,
            'protype' => $this->getProductTypeTable()->getProductType($id)
        );
    }

}