<?php
/**
 * ProductController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Product\Model\Product;
use Product\Form\ProductForm;
use Login\Controller\LoginController;

class ProductController extends AbstractActionController
{
    protected $productTable;
    protected $dbAdapter;
    protected $config;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }
        return $this->config;
    }

    public function getDbAdapter() {

        if(!$this->dbAdapter) {
            $this->dbAdapter = $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter');
        }
        return $this->dbAdapter;

    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }

    public function handleUpload($files, $product_id)
    {
        $config = $this->getConfig();
        $name = null;
        $last_id = null;
        // handle upload
        if(isset($files['images'])) {

            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions

            if(isset($files['banner']) && $files['banner']['error']==UPLOAD_ERR_OK) {
                $file = $files['banner'];
                $fileParts = pathinfo($file['name']);
                if (in_array(strtolower($fileParts['extension']),$fileTypes)) {

                    if(!is_dir($config['product_upload_folder'] . 'banner/')) {
                        mkdir($config['product_upload_folder'] . 'banner/');
                        chmod($config['product_upload_folder'] . 'banner/', 0775);
                    }
                    $name = $product_id . ".jpg";
                    $origin = $file['tmp_name'];
                    $target = $config['product_upload_folder'] . 'banner/' . $name;

                    $w = \WideImage::load($origin);
                    $w->saveToFile($target);

                }
            }


            foreach($files['images'] as $file) {
                $fileParts = pathinfo($file['name']);
                if (in_array(strtolower($fileParts['extension']), $fileTypes)) {

                    $mod = '';
                    do {
                        $name = $product_id . ($mod == '' ? '' : '_' . $mod) . '.' . $fileParts['extension'];
                        $target = $config['product_upload_folder'];
                        $mod++;
                    } while (file_exists($target . 'original/' . $name));

                    $origin = $file['tmp_name'];

                    foreach($config['product_img_sizes'] as $size => $opts) {
                        if($size!='original') {
                            $w = \WideImage::load($origin);

                            if (!is_null($opts['resize'])) {
                                $w = $w->resize($opts['w'], $opts['h'], $opts['resize']);
                            }
                            if (!is_null($opts['crop'])) {

                                $w = $w->crop($opts['crop'], $opts['crop'], $opts['w'], $opts['h']);
                            }
                            $w->saveToFile($config['product_upload_folder'] . $size . '/' . $name);
                        }
                    }
                    move_uploaded_file($origin, $target . 'original/' . $name);
                    $last_id = $this->getProductTable()->addImage($product_id, $name);
                }
            }
        }
        return $last_id;
    }


    public function removeimageAction() {

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('product', array('action' => 'add'));
        }

        $config = $this->getConfig();
        $record = $this->getProductTable()->getImage($id);

        foreach($config['product_img_sizes'] as $size => $opts) {
            $path = $config['product_upload_folder'] . $size . '/' . $record->file;
            @unlink($path);
        }
        $this->getProductTable()->removeImage($id);

        $this->layout('layout/json');
        return new ViewModel(array(
            'data' => array('msg' => 'Imagem removida.')
        ));

    }

    public function indexAction()
    {
        LoginController::checkAuthentication();

        return new ViewModel(array(
            'products' => $this->getProductTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new ProductForm( $this->getDbAdapter(), 0 );

        $request = $this->getRequest();
        if ($request->isPost()) {
            $product = new Product();
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $product->exchangeArray($form->getData());
                $id = $this->getProductTable()->saveProduct($product);

                $categories = $request->getPost('category_id', array());
                $this->getProductTable()->updateCategories($id, $categories);

                $product->main_image_id = $this->handleUpload($request->getFiles()->toArray(), $id);
                $this->getProductTable()->saveProduct($product);

                // Redirect to list of products
                return $this->redirect()->toRoute('product');
            }
        }
        return array(
            'form' => $form,
            'categories' => $this->getServiceLocator()->get('Product\Model\ProcatTable')->fetchAll(),
            'config' => $this->getConfig()
        );
    }

    public function editAction()
    {
        LoginController::checkAuthentication();


        $id = (int) $this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('product', array('action' => 'add'));
        }

        try {
            $product = $this->getProductTable()->getProduct($id);

        }
        catch (\Exception $ex) {

            return $this->redirect()->toRoute('product', array('action' => 'index'));
        }

        $form  = new ProductForm($this->getDbAdapter(), $id);
        $form->bind($product);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getProductTable()->saveProduct($product);
                $categories = $request->getPost('category_id', array());
                $this->getProductTable()->updateCategories($id, $categories);

                // handle uploads
                $this->handleUpload($request->getFiles()->toArray(), $id);

                // Redirect to list of products
                return $this->redirect()->toRoute('product', array('action'=>'index'));
            }
        }

        $config = $this->getConfig();

        return array(
            'id' => $id,
            'form' => $form,
            'product' => $product,
            'img_folder' => $config['product_img_folder'],
            'categories' => $this->getProductTable()->getProductCategories($id),
            'images' => $this->getProductTable()->getImages($id),
            'config' => $this->getConfig()
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('product');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {
                $id = (int) $request->getPost('id');
                $this->getProductTable()->deleteProduct($id);
            }

            // Redirect to list of products
            return $this->redirect()->toRoute('product');
        }

        return array(
            'id'    => $id,
            'product' => $this->getProductTable()->getProduct($id)
        );
    }

    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Product\Model\ProductTable');
        }
        return $this->productTable;
    }

    protected $productCategoryTable;
    public function getProductCategoryTable()
    {

        if (!$this->productCategoryTable) {
            $sm = $this->getServiceLocator();
            $this->productCategoryTable = $sm->get('Product\Model\ProductCategoryTable');
        }
        return $this->productCategoryTable;
    }
}