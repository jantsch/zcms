<?php
/**
 * ProcatController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Product\Model\Procat;
use Product\Form\ProcatForm;
use Login\Controller\LoginController;

class ProcatController extends AbstractActionController
{
    protected $procatTable;
    protected $config;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }
        return $this->config;
    }


    public function getProcatTable()
    {
        if (!$this->procatTable) {
            $sm = $this->getServiceLocator();
            $this->procatTable = $sm->get('Product\Model\ProcatTable');
        }
        return $this->procatTable;
    }
    public function indexAction()
    {
        LoginController::checkAuthentication();

        return new ViewModel(array(
            'procats' => $this->getProcatTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new ProcatForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $procat = new Procat();
            $form->setInputFilter($procat->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $procat->exchangeArray($form->getData());
                $id = $this->getProcatTable()->saveProcat($procat);

                // Redirect to list of procats
                return $this->redirect()->toRoute('procat');
            }
        }
        return array(
            'form' => $form
        );
    }

    public function editAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('procat', array(
                'action' => 'add'
            ));
        }

        try {
            $procat = $this->getProcatTable()->getProcat($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('procat', array(
                'action' => 'index'
            ));
        }

        $form  = new ProcatForm();
        $form->bind($procat);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($procat->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getProcatTable()->saveProcat($procat);

                // Redirect to list of procats
                return $this->redirect()->toRoute('procat');
            }
        }

        $config = $this->getConfig();

        return array(
            'id' => $id,
            'form' => $form
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('procat');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {

                $id = (int) $request->getPost('id');
                $this->getProcatTable()->deleteProcat($id);
            }

            // Redirect to list of procats
            return $this->redirect()->toRoute('procat');
        }

        return array(
            'id'    => $id,
            'procat' => $this->getProcatTable()->getProcat($id)
        );
    }
}