<?php
/**
 * Post.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

Namespace Product\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductType implements InputFilterAwareInterface
{
    public $id;
    public $title;
    public $content;
    public $slug;
    public $created_on;
    public $modified_on;
    public $created_by;
    public $modified_by;
    public $disabled_by;

    protected $inputFilter;
    protected $dbAdapter;

    public function setDbAdapter($adapter){

        $this->dbAdapter = $adapter;
    }

    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->title  = (!empty($data['title'])) ? $data['title'] : null;
        $this->content  = (!empty($data['content'])) ? $data['content'] : null;
        $this->slug  = (!empty($data['slug'])) ? $data['slug'] : null;
        $this->created_on  = (!empty($data['created_on'])) ? $data['created_on'] : null;
        $this->created_by  = (!empty($data['created_by'])) ? $data['created_by'] : null;
        $this->modified_on  = (!empty($data['modified_on'])) ? $data['modified_on'] : null;
        $this->modified_by  = (!empty($data['modified_by'])) ? $data['modified_by'] : null;
        $this->disabled_by  = (!empty($data['disabled_by'])) ? $data['disabled_by'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $id = isset($this->data['id']) && intval($this->data['id'])>0 ? $this->data['id'] : intval($this->id);

            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                    array(
                        'name' => '\Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'product_type',
                            'field' => 'title',
                            'adapter' => $this->dbAdapter,
                            'exclude' => array('field' => 'id', 'value' => $id),
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Linha já existe',
                            ),
                        ),
                    ),
                )
            ));


            $inputFilter->add(array(
                'name'     => 'content',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StringTrim'),
                )
            ));


            $inputFilter->add(array(
                'name'     => 'slug',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                    array(
                        'name' => '\Zend\Validator\Db\NoRecordExists',
                        'options' => array(
                            'table' => 'slug',
                            'field' => 'slug',
                            'adapter' => $this->dbAdapter,
                            'exclude' => array('field' => 'product_type_id', 'value' => $id),
                            'messages' => array(
                                \Zend\Validator\Db\NoRecordExists::ERROR_RECORD_FOUND => 'Endereço em uso',
                            ),
                        ),
                    ),
                ),
            ));

           $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
