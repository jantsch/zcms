<?php
/**
 * ProcatTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;
use Zend\Db\Sql\Where;

class ProcatTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getProcat($id)
    {
        if( (int)$id >0) {
            $id=(int)$id;
            $rowset = $this->tableGateway->select(array('id' => $id));

        }elseif(!empty($id)) {
            $rowset = $this->tableGateway->select(array('title' => $id));

        }else{
            return null;
        }

        $row = $rowset->current();
        return $row;

    }

    public function saveProcat(Procat $procat)
    {
        $data = array(
            'title'  => $procat->title,
        );

        $id = (int) $procat->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $this->tableGateway->getLastInsertValue();
            return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('procat_id_seq');

        } else {
            if ($this->getProcat($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Procat id does not exist');
            }
        }

    }

    public function deleteProcat($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }

}