<?php
/**
 * ProductTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;
use Slug\Model\Slug;
use Slug\Model\SlugTable;
use Zend\Db\Sql\Where;

class ProductTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $slugTable = null;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function getSlugTable() {

        if($this->slugTable==null ) {
            $this->slugTable = $this->serviceLocator->get('Slug\Model\SlugTable');
        }
        return $this->slugTable;
    }

    public function resultsetToArray($resultset) {
        $return = array();
        foreach($resultset as $row) {
            $return[] = $row;
        }

        return $return;
    }

    public function fetchAll($ids=null)
    {
        $resultSet = $this->tableGateway->select(function($select) use($ids){
            $select->join('slug', 'slug.product_id = product.id', array('slug'), 'left');
            $select->join('product_image', 'product_image.id = product.main_image_id', array('file'), 'left');
            $select->join('product_type', 'product_type.id = product.type_id', array('type_title'=>'title'), 'left');
            $select->order('name ASC, id ASC');

            if(!is_null($ids)) {
                $w = new Where();
                if(is_array($ids)) {
                    $w->in('product.id', $ids);
                }else{
                    $w->equalTo('product.id', $ids);
                }
                $select->where($w);
            }

        });

        return $resultSet;
    }

    public function getSearch($term, $page)
    {
        $resultSet = $this->tableGateway->select(function($select) use($term){
            $select->columns(array('*', "title"=> "name"));
            $select->join('slug', 'slug.product_id = product.id', array('slug'), 'left');
            $select->join('product_type', 'product_type.id = product.type_id', array('type_title'=>'title'), 'left');
            $select->order('name ASC, id ASC');

            $w = new Where();
            $w->like('product.name', "%$term%");
            $select->where($w);

        });
        //var_dump($resultSet);die;
        return $resultSet;
    }

    protected $fetchProductsByType_cache = array();

    public function fetchProductsByType($type_id, $page=0, $page_length=0)
    {
        if(!isset($this->fetchProductsByType_cache[$type_id])) {

            $resultSet = $this->tableGateway->select(function ($select) use ($type_id, $page_length) {
                $select->join('slug', 'slug.product_id = product.id', array('slug'), 'left');
                $select->join('product_image', 'product_image.id = product.main_image_id', array('file'), 'left');
                $select->where(array('type_id = ' . (int)$type_id));
                $select->order('name ASC');
                if($page_length>0) {
                    $select->limit($page_length);
                }

            });
            $this->fetchProductsByType_cache[$type_id] = $this->resultsetToArray($resultSet);
        }
        return $this->fetchProductsByType_cache[$type_id];
    }

    public function getProduct($id)
    {
        $id  = (int) $id;

        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('*'));
        $sqlSelect->join('slug', 'slug.product_id = product.id', array('slug'), 'left');

        $w = new Where();
        $w->equalTo('product.id', $id);
        $sqlSelect->where($w);

        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($sqlSelect);

        $resultSet = $statement->execute();
        $row = $resultSet->current();
        $product = new Product();
        $product->exchangeArray($row);

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $product;
    }

    public function saveProduct(Product $product)
    {
        $data = array(
            'name' => $product->name,
            'type_id' => $product->type_id,
            'main_image_id' => $product->main_image_id,
            'status' => $product->status,
            'description'  => $product->description,
            'info' => $product->info,
            'tips' => $product->tips,
        );

        $id = (int) $product->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $this->tableGateway->getLastInsertValue();
            $product->id = $this->tableGateway->getLastInsertValue();
            //$product->id = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('product_id_seq');

        } else {
            if ($this->getProduct($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Product id does not exist');
            }
        }

        $tslug = $this->getSlugTable();
        $slug = $tslug->getSlugFor('product', $product->id);

        if(is_null($slug)) {
            $slug = new Slug();
        }
        $slug->slug = $product->slug;
        $slug->product_id = $product->id;
        $tslug->saveSlug($slug);

        return $product->id;

    }

    public function addImage($product_id, $file_name) {

        $product_id = (int)$product_id;
        if($product_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "INSERT INTO product_image (product_id, file, created_by) VALUES (?, ?, ?)";
        $ad->query($sql, array($product_id, $file_name, (int)LoginController::loggedUser('id') ));
        return $ad->getDriver()->getConnection()->getLastGeneratedValue();

    }

    public function getRelatedIds($product_id) {

        $product_id = (int)$product_id;
        if($product_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT group_concat(pp.product_id) AS ids
              FROM product_procat p
              LEFT JOIN product_procat pp ON p.procat_id=pp.procat_id
              WHERE p.product_id=? AND pp.product_id<>?";
        $resultSet = $ad->query($sql, array($product_id, $product_id));
        return $resultSet->current()->ids;

    }

    public function getImages($product_id) {

        $product_id = (int)$product_id;
        if($product_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT * FROM product_image WHERE product_id=?";
        $resultSet = $ad->query($sql, array($product_id));
        return $resultSet;

    }

    public function getImage($image_id) {

        $image_id = (int)$image_id;
        if($image_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT * FROM product_image WHERE id=?";
        $resultSet = $ad->query($sql, array($image_id));
        return $resultSet->current();

    }

    public function removeImage($image_id) {

        $image_id = (int)$image_id;
        if($image_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "DELETE FROM product_image WHERE id=?";
        $resultSet = $ad->query($sql, array($image_id));
        return $resultSet;

    }

    public function updateCategories($product_id, $procat_id = array()) {

        $product_id = (int)$product_id;
        if($product_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "DELETE FROM product_procat WHERE product_id = ?";
        $resultSet = $ad->query($sql, array($product_id));

        foreach($procat_id as $cid) {
            $cid = (int)$cid;
            if($cid>0) {
                $sql = "INSERT INTO product_procat (product_id, procat_id) VALUES (?, ?)";
                $resultSet = $ad->query($sql, array($product_id, $cid));
            }
        }
    }

    public function getProductCategories($product_id, $selected_only = false) {

        $product_id = (int)$product_id;
        if($product_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT  procat.id, procat.title, x.product_id
                FROM procat
                 LEFT JOIN ( SELECT * FROM product_procat WHERE product_id=?) AS x ON procat.id=x.procat_id
                 " . ($selected_only ? 'WHERE product_id IS NOT NULL' : '') . " ORDER BY procat.title";

        $resultSet = $ad->query($sql, array($product_id));

        return $resultSet;
    }

    public function getProductsByCategory($procat_id) {

        $procat_id = (int)$procat_id;
        if($procat_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT product.*, product_procat.procat_id, procat.title, slug.slug
                FROM product_procat
                  JOIN product  ON product.id=product_procat.product_id
                  JOIN procat ON product_procat.procat_id=procat.id
                  LEFT JOIN slug ON product.id=slug.product_id
              WHERE product_procat.procat_id=? ORDER BY procat.title";

        $resultSet = $ad->query($sql, array($procat_id));

        return $resultSet;
    }

    public function deleteProduct($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}