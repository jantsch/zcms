<?php
/**
 * ProductTypeTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;
use Slug\Model\Slug;
use Slug\Model\SlugTable;
use Zend\Db\Sql\Where;

class ProductTypeTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $slugTable = null;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function getSlugTable() {

        if($this->slugTable==null ) {
            $this->slugTable = $this->serviceLocator->get('Slug\Model\SlugTable');
        }
        return $this->slugTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select){
            $select->join('slug', 'slug.product_type_id = product_type.id', array('slug'));
            $select->order('title ASC');
        });

        $return = array();
        foreach($resultSet as $row) {
            $return[] = $row;
        }

        return $return;
    }

    public function getProductType($id)
    {
        $id  = (int) $id;

        $sqlSelect = $this->tableGateway->getSql()->select();
        $sqlSelect->columns(array('*'));
        $sqlSelect->join('slug', 'slug.product_type_id = product_type.id', array('slug'), 'left');

        $w = new Where();
        $w->equalTo('product_type.id', $id);
        $sqlSelect->where($w);

        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($sqlSelect);

        $resultSet = $statement->execute();
        $row = $resultSet->current();
        $protype = new ProductType();
        $protype->exchangeArray($row);

        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $protype;
    }

    public function saveProductType(ProductType $protype)
    {
        $data = array(
            'title' => $protype->title,
            'content' => $protype->content
        );

        $id = (int) $protype->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $protype->id = $this->tableGateway->getLastInsertValue();

        } else {
            if ($this->getProductType($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('ProductType id does not exist');
            }
        }

        $tslug = $this->getSlugTable();
        $slug = $tslug->getSlugFor('product_type', $protype->id);

        if(is_null($slug)) {
            $slug = new Slug();
        }
        $slug->slug = $protype->slug;
        $slug->product_type_id = $protype->id;
        $tslug->saveSlug($slug);

        return $protype->id;

    }

    public function deleteProductType($id)
    {
        $id = (int) $id;
        $this->tableGateway->delete(array('id' => $id));
        $this->getSlugTable()->deleteSlugFor('product_type', $id);
    }
}