<?php
/**
 * ProductForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Form;

use Zend\Form\Form;

class ProductTypeForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('producttype');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'slug',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'content',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'file',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'logo',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}