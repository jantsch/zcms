<?php
/**
 * ProductForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Product\Form;

use Zend\Form\Form;
use Product\Model\ProductTypeTable;

class ProductForm extends Form
{
    protected $dbAdapter;

    public function __construct($dbAdapter = null, $product_id)
    {
        $this->dbAdapter = $dbAdapter;

        // we want to ignore the name passed
        parent::__construct('product');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'description',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'slug',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    'enabled' => 'ativo',
                    'disabled' => 'inativo'
                )
            )
        ));

        $types =

        $this->add(array(
            'name' => 'type_id',
            'type' => 'Select',
            'options' => array(
                'value_options' => $this->getOptionsForSelect($product_id)
            )
        ));

        $this->add(array(
            'name' => 'info',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'tips',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'images',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'banner',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'main_image_id',
            'type' => 'hidden',
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }

    public function getOptionsForSelect($product_id)
    {

        $sql       = "SELECT product_type.id, product_type.title, pro.product_id
            FROM product_type
            LEFT JOIN (SELECT id product_id, type_id FROM product WHERE id=?) pro ON product_type.id=pro.type_id
            ORDER BY title ASC";
        $statement = $this->dbAdapter->query($sql);
        $result    = $statement->execute(array($product_id));

        $selectData = array();

        foreach ($result as $res) {

            $row = array('label'=> $res['title'], 'value'=>$res['id']);
            if((int)$res['product_id']>0) {
                $row['selected'] = true;
            }
            $selectData[] = $row;
        }
        return $selectData;
    }
}