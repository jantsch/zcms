<?php
/**
 * BannerForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Banner\Form;

use Zend\Form\Form;

class BannerForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('banner');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'header',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'href',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    'public' => 'ativo',
                    'private' => 'inativo'
                )
            )
        ));

        $this->add(array(
            'name' => 'posicao',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    'normal' => 'Normal',
                    'topo' => 'Topo'
                )
            )
        ));

        $this->add(array(
            'name' => 'file',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}