<?php
/**
 * BannerTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Banner\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;

class BannerTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Service locator may be used to access other tables.
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getBanner($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getActiveBanners()
    {
        return $this->tableGateway->select(array('status' => 'public', 'posicao' => 'normal'));

    }

    public function getTopBanner()
    {
        return $this->tableGateway->select(array('status' => 'public', 'posicao' => 'topo'))->current();

    }

    public function saveBanner(Banner $banner)
    {
        $data = array(
            'title'  => $banner->title,
            'href'  => $banner->href,
            'header'  => $banner->header,
            'status' => $banner->status,
            'posicao' => $banner->posicao
        );

        // update file only when required

        if(!empty($banner->file)) {
            $data['file'] = $banner->file;
        }

        $id = (int) $banner->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        } else {
            if ($this->getBanner($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Banner id does not exist');
            }
        }

    }

    public function deleteBanner($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}