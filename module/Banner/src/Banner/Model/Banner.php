<?php
/**
 * Banner.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Banner\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Banner implements InputFilterAwareInterface
{
    public $id;
    public $title;
    public $header;
    public $href;
    public $file;
    public $status;
    public $posicao;
    public $created_on;
    public $modified_on;
    public $created_by;
    public $modified_by;
    public $disabled_by;

    protected $inputFilter;
    protected $dbAdapter;

    public function setDbAdapter($adapter){

        $this->dbAdapter = $adapter;
    }

    public function exchangeArray($data)
    {
        $this->id     = (!empty($data['id'])) ? $data['id'] : null;
        $this->title  = (!empty($data['title'])) ? $data['title'] : null;
        $this->header = (!empty($data['header'])) ? $data['header'] : null;
        $this->href = (!empty($data['href'])) ? $data['href'] : null;
        $this->file = (!empty($data['file'])) ? $data['file'] : null;
        $this->status  = (!empty($data['status'])) ? $data['status'] : null;
        $this->posicao  = (!empty($data['posicao'])) ? $data['posicao'] : null;
        $this->created_on  = (!empty($data['created_on'])) ? $data['created_on'] : null;
        $this->created_by  = (!empty($data['created_by'])) ? $data['created_by'] : null;
        $this->modified_on  = (!empty($data['modified_on'])) ? $data['modified_on'] : null;
        $this->modified_by  = (!empty($data['modified_by'])) ? $data['modified_by'] : null;
        $this->disabled_by  = (!empty($data['disabled_by'])) ? $data['disabled_by'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'title',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                )
            ));

            $inputFilter->add(array(
                'name'     => 'header',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                )
            ));
            $inputFilter->add(array(
                'name'     => 'href',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 255,
                        ),
                    ),
                )
            ));

            $inputFilter->add(array(
                'name'     => 'status',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
