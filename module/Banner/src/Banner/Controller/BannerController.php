<?php
/**
 * BannerController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Banner\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Banner\Model\Banner;
use Banner\Form\BannerForm;
use Login\Controller\LoginController;

class BannerController extends AbstractActionController
{
    protected $bannerTable;
    protected $config;
    protected $fileWasUploaded;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }
        return $this->config;
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
        $text = trim($text, '-');
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower($text);
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }
    public function indexAction()
    {
        LoginController::checkAuthentication();

        return new ViewModel(array(
            'banners' => $this->getBannerTable()->fetchAll(),
        ));
    }

    public function handleUpload($files, $title, $old_file = '')
    {
        $this->fileWasUploaded = false;
        $config = $this->getConfig();
        $name = null;
        // handle upload
        if(isset($files['file'])) {
            $file = $files['file'];

            $fileTypes = array('jpg','jpeg','gif','png');
            $fileParts = pathinfo($file['name']);
            if (in_array(strtolower($fileParts['extension']),$fileTypes)) {
                if($old_file!='' && file_exists($config['banners_upload_folder'] . $old_file)) {
                    @unlink($config['banners_upload_folder'] . $old_file);
                }

                $mod = '';
                do {
                    $name = $this->slugify($title) . ($mod=='' ? '' : '_' .$mod) . '.' . $fileParts['extension'];
                    $target = $config['banners_upload_folder'] . $name;
                    $mod++;
                }while(file_exists($target));

                $origin = $file['tmp_name'];
                move_uploaded_file($origin, $target);
                $this->fileWasUploaded = true;
            }
        }

        return $name;
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $config = $this->getConfig();
        $form = new BannerForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $banner = new Banner();
            $form->setInputFilter($banner->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $banner->exchangeArray($form->getData());
                $banner->file = $this->handleUpload( $request->getFiles()->toArray(), $request->getPost('title', ''));
                $id = $this->getBannerTable()->saveBanner($banner);

                // Redirect to list of banners
                if($this->fileWasUploaded) {
                    return $this->redirect()->toRoute('banner', array('action' => 'edit', 'id' => $id));
                }
                return $this->redirect()->toRoute('banner');
            }
        }

        return array(
            'form' => $form,
            'banners_img_folder' => $config['banners_img_folder']
        );
    }

    public function editAction()
    {
        LoginController::checkAuthentication();

        $config = $this->getConfig();
        $id = (int)$this->params()->fromRoute('id', 0);
        if($id==0) {
            $id = $this->getRequest()->getPost('id', 0);
        }
        if (!$id) {

            return $this->redirect()->toRoute('banner', array(
                'action' => 'add'
            ));
        }

        // Get the Banner with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index banner.
        try {
            $banner = $this->getBannerTable()->getBanner($id);
            $old_file = $banner->file;
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('banner', array(
                'action' => 'index'
            ));
        }

        $form  = new BannerForm();
        $form->bind($banner);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($banner->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $banner->file = $this->handleUpload( $request->getFiles()->toArray(), $request->getPost('title', ''), $old_file);
                $this->getBannerTable()->saveBanner($banner);

                // Redirect to list of banners
                if($this->fileWasUploaded) {
                    return $this->redirect()->toRoute('banner', array('action'=>'edit', 'id'=>$id));
                }
                return $this->redirect()->toRoute('banner');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
            'banners_img_folder' => $config['banners_img_folder']
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('banner');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {
                $id = (int) $request->getPost('id');
                $this->getBannerTable()->deleteBanner($id);
            }

            // Redirect to list of banners
            return $this->redirect()->toRoute('banner');
        }

        return array(
            'id'    => $id,
            'banner' => $this->getBannerTable()->getBanner($id)
        );
    }

    public function getBannerTable()
    {
        if (!$this->bannerTable) {
            $sm = $this->getServiceLocator();
            $this->bannerTable = $sm->get('Banner\Model\BannerTable');
        }
        return $this->bannerTable;
    }
}