<?php
/**
 * module.config.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Banner\Controller\Banner' => 'Banner\Controller\BannerController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'banner' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/banner[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Banner\Controller\Banner',
                        'action'     => 'index',
                    ),
                ),
            ),
            'banner_home' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/admin/banner/',
                    'defaults' => array(
                        'controller' => 'Banner\Controller\Banner',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'banner' => __DIR__ . '/../view',
        ),
    ),
);