<?php
/**
 * Post.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PostComment implements InputFilterAwareInterface
{
    public $id;
    public $name;
    public $email;
    public $subject;
    public $message;
    public $status;
    public $user_id;
    public $first_name;
    public $post_id;
    public $slug;
    public $created_on;
    public $reply_to;
    public $modified_on;
    public $created_by;
    public $modified_by;
    public $disabled_by;

    protected $inputFilter;
    protected $dbAdapter;

    public function setDbAdapter($adapter){

        $this->dbAdapter = $adapter;
    }

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->subject = (!empty($data['subject'])) ? $data['subject'] : null;
        $this->message = (!empty($data['message'])) ? $data['message'] : null;
        $this->statu = (!empty($data['status'])) ? $data['status'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->user_id = (!empty($data['user_id'])) ? $data['user_id'] : null;
        $this->first_name = (!empty($data['first_name'])) ? $data['first_name'] : null;
        $this->post_id = (!empty($data['post_id'])) ? $data['post_id'] : null;
        $this->slug = (!empty($data['slug'])) ? $data['slug'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
        $this->reply_to = (!empty($data['reply_to'])) ? $data['reply_to'] : null;
        $this->created_on = (!empty($data['created_on'])) ? $data['created_on'] : null;
        $this->modified_on = (!empty($data['modified_on'])) ? $data['modified_on'] : null;
        $this->modified_by = (!empty($data['modified_by'])) ? $data['modified_by'] : null;
    }


    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }


    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $id = isset($this->data['id']) && intval($this->data['id'])>0 ? $this->data['id'] : intval($this->id);

            $inputFilter->add(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'user_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'post_id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'reply_to',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            ));

            $inputFilter->add(array(
                'name'     => 'name',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                )
            ));

            $inputFilter->add(array(
                'name'     => 'email',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                )
            ));

            $inputFilter->add(array(
                'name'     => 'subject',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                )
            ));

            $inputFilter->add(array(
                'name'     => 'message',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags')
                )
            ));


            $inputFilter->add(array(
                'name'     => 'status',
                'required' => true,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 1,
                            'max'      => 100,
                        ),
                    ),
                ),
            ));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
