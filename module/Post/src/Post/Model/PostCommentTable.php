<?php
/**
 * PostTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class PostCommentTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select) {
            $select->join('user', 'user.id = post_comment.user_id', array('first_name'), 'left');
            $select->join('post', 'post.id = post_comment.post_id', array('title'), 'left');
            $select->order('created_on DESC');
        });
        return $resultSet;
    }

    public function getByStatus($status, $post_id = 0, $order='published_on ASC')
    {

        $post_id = (int)$post_id;
        $resultSet = $this->tableGateway->select(function($select) use($post_id, $status, $order){

            $w = new Where();
            if ($post_id > 0) {
                $w->equalTo('post_comment.post_id', $post_id);
            }
            $w->equalTo('post_comment.status', $status);

            $select->join('user', 'user.id = post_comment.user_id', array('first_name'), 'left');
            $select->join('post', 'post.id = post_comment.post_id', array('title'), 'left');
            $select->join('slug', 'slug.post_id = post_comment.post_id', array('slug'), 'left');
            $select->order($order);
            $select->where($w);
        });

       return $resultSet;

    }

    public function getApproved($comment_id)
    {

        $resultSet = $this->tableGateway->select(function($select) use($comment_id) {

            $w = new Where();
            $w->equalTo('post_id', (int)$comment_id);
            $w->equalTo('status', 'approved');

            $select->join('user', 'user.id = post_comment.user_id', array('first_name'), 'left');
            $select->join('post', 'post.id = post_comment.post_id', array('title'), 'left');
            $select->where($w);
            $select->order('created_on ASC');
        });

       return $resultSet;

    }

    public function getPostComment($id)
    {
        $resultSet = $this->tableGateway->select(function($select) use($id){

            $w = new Where();
            $w->equalTo('post_comment.id', $id);

            $select->join('user', 'user.id = post_comment.user_id', array('first_name'), 'left');
            $select->join('post', 'post.id = post_comment.post_id', array('title'), 'left');
            $select->where($w);

        });

        return $resultSet->current();

    }

    public function savePostComment(PostComment $comment)
    {
        $data = array(
            'name' => $comment->name,
            'email' => $comment->email,
            'subject'  => $comment->subject,
            'message' => $comment->message,
            'status' => $comment->status,
            'post_id' => $comment->post_id,
            'reply_to' => $comment->reply_to
        );

        $id = (int) $comment->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['user_id'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $comment->id = $this->tableGateway->getLastInsertValue();

        } else {
            if ($this->getPostComment($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Pae id does not exist');
            }
        }

        return $comment->id;

    }

    public function updateStatus($comment_ids, $status) {

        $comment_ids (is_array($comment_ids) ? $comment_ids : array((int)$comment_ids));
        $status = strtolower($status);
        $resultSet = null;

        if(in_array($status, array('review', 'approved', 'rejected'))) {

            foreach($comment_ids as &$i) {
                $i = (int)$i;
            }
            unset($i);

            $in = implode(','. $comment_ids);

            $sql = "UPDATE post_comment SET status='?' WHERE id IN (?)";
            $resultSet = $this->tableGateway->getAdapter()->query($sql, array($status, $in));

        }
        return $resultSet;
    }

    public function deletePostComment($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}