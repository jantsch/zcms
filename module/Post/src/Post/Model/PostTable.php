<?php
/**
 * PostTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;
use Login\Controller\LoginController;
use Slug\Model\Slug;
use Slug\Model\SlugTable;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql;
class PostTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $slugTable = null;
    protected $tableGateway;
    public static $foundRows;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function getSlugTable() {

        if($this->slugTable==null ) {
            $this->slugTable = $this->serviceLocator->get('Slug\Model\SlugTable');
        }
        return $this->slugTable;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select) {
            $select->order('published_on DESC');
        });
        return $resultSet;
    }

    public function getPublished($page, $page_length=5, $order_by='published_on DESC')
    {
        $start = ((($page-1)>=0) ? ($page - 1) :0)*$page_length;


        $resultSet = $this->tableGateway->select(function($select) use ($start, $page_length, $order_by){
            $w = new Where();
            $w->equalTo('status', 'published');
            $w->in('post_categories.category_id', array(8,9,10));

            $select->join('slug', 'slug.post_id = post.id', array('slug'), 'left');
            $select->join('user', 'user.id = post.created_by', array('first_name'), 'left');
            $select->join('post_categories', 'post.id = post_categories.post_id', array('post_id'), 'left');
            $select->join('category', 'category.id = post_categories.category_id', array('category_title'=>new \Zend\Db\Sql\Expression('GROUP_CONCAT(category.title)')), 'left');
            $select->group('post.id');

            $select->where($w);
            if(!empty($order_by)) {
                $select->order($order_by);
            }
            $select->limit($page_length)->offset($start)->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        });
       // var_dump($resultSet->current()->category_title);die;


        $sql = "SELECT FOUND_ROWS() AS rows";
        $foundRows = $this->tableGateway->getAdapter()->query($sql, array())->toArray();
        self::$foundRows = isset($foundRows[0]['rows']) ? (int)$foundRows[0]['rows'] : 0;

        // populate post with categories
        $ret = array();
        foreach($resultSet as $row) {

            $ad = $this->tableGateway->getAdapter();
            $sql = "SELECT category.id, category.title, slug.slug
                  FROM post_categories
                    LEFT JOIN category  ON post_categories.category_id= category.id
                    LEFT JOIN slug ON category.id=slug.category_id
                WHERE post_categories.post_id = ?";
            $categories = $ad->query($sql, array($row->id));

            $row->categories = array();
            foreach($categories as $category) {
                $row->categories[] = $category;
                $row->category_title .= (empty($row->category_title) ? '' : ', ') . $category['title'];
            }
            $ret[] = $row;

        }
        return $ret;

    }

    public function getByTag($tag, $page, $page_length=5, $order_by='published_on DESC')
    {
        $start = (($page-1>=0) ? $page - 1 :0) * $page_length;

        $resultSet = $this->tableGateway->select(function($select) use ($tag, $start, $page_length, $order_by){
            $w = new Where();
            $w->equalTo('status', 'published');
            $w->equalTo('post_tag.tag', $tag);

            $select->join('slug', 'slug.post_id = post.id', array('slug'), 'left');
            $select->join('user', 'user.id = post.created_by', array('first_name'), 'left');
            $select->join('post_categories', 'post.id = post_categories.post_id', array('post_id'), 'left');
            $select->join('category', 'category.id = post_categories.category_id', array('category_title'=>new \Zend\Db\Sql\Expression('GROUP_CONCAT(category.title)')), 'left');
            $select->join('post_tag', 'post.id = post_tag.post_id', array('tags'=>new \Zend\Db\Sql\Expression('GROUP_CONCAT(post_tag.tag)')), 'left');
            $select->group('post.id');

            $select->where($w);
            if(!empty($order_by)) {
                $select->order($order_by);
            }
            $select->limit($page_length)->offset($start)->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        });

        $sql = "SELECT FOUND_ROWS() AS rows";
        $foundRows = $this->tableGateway->getAdapter()->query($sql, array())->toArray();
        self::$foundRows = isset($foundRows[0]['rows']) ? (int)$foundRows[0]['rows'] : 0;

        return $resultSet;

    }

    public function getMostRecent($page, $page_length=3, $order_by='published_on DESC')
    {
        $start = (($page-1>=0) ? $page - 1 :0) * $page_length;

        $resultSet = $this->tableGateway->select(function($select) use ($start, $page_length, $order_by){
            $w = new Where();
            $w->equalTo('status', 'published');

            $select->join('slug', 'slug.post_id = post.id', array('slug'), 'left');
            $select->join('user', 'user.id = post.created_by', array('first_name'), 'left');
            $select->join('post_categories', 'post.id = post_categories.post_id', array('post_id'), 'left');
            $select->join('category', 'category.id = post_categories.category_id', array('category_title'=>new \Zend\Db\Sql\Expression('GROUP_CONCAT(category.title)')), 'left');
            $select->group('post.id');

            $select->where($w);
            if(!empty($order_by)) {
                $select->order($order_by);
            }
            $select->limit($page_length);
            $select->offset($start);
        });

        return $resultSet;

    }

    public function getSearch($term, $page, $page_length=50, $order_by='published_on DESC')
    {
        $start = max(0, ($page-1)) * $page_length;

        $resultSet = $this->tableGateway->select(function($select) use ($term, $start, $page_length, $order_by){
            $w = new Where();
            $w->equalTo('status', 'published');
            $w->like('post.title', "%$term%");

            $select->join('slug', 'slug.post_id = post.id', array('slug'), 'left');
            $select->join('user', 'user.id = post.created_by', array('first_name'), 'left');
            $select->join('post_categories', 'post.id = post_categories.post_id', array('post_id'), 'left');
            $select->join('category', 'category.id = post_categories.category_id', array('category_title'=>new \Zend\Db\Sql\Expression('GROUP_CONCAT(category.title)')), 'left');
            $select->group('post.id');

            $select->where($w);
            if(!empty($order_by)) {
                $select->order($order_by);
            }
            $select->limit($page_length)->offset($start)->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        });

        $sql = "SELECT FOUND_ROWS() AS rows";
        $foundRows = $this->tableGateway->getAdapter()->query($sql, array())->toArray();
        self::$foundRows = isset($foundRows[0]['rows']) ? (int)$foundRows[0]['rows'] : 0;

        return $resultSet;
    }


    public function getPostsByCategory($id, $page=0, $page_length=50) {

        $start = (($page-1>=0) ? $page - 1 :0) * $page_length;
        $id  = (int) $id;

        $ret = $this->tableGateway->select(function($select) use ($id, $start, $page_length){
            $select->columns(array('*'));
            $select->join('post_categories', 'post_categories.post_id = post.id', array('category_id'), 'left');
            $select->group('post.id');

            $w = new Where();
            $w->equalTo('post_categories.category_id', $id);
            $select->where($w);

            $select->limit($page_length)->offset($start)->quantifier(new Expression('SQL_CALC_FOUND_ROWS'));
        });

        $sql = "SELECT FOUND_ROWS() AS rows";
        $foundRows = $this->tableGateway->getAdapter()->query($sql, array())->toArray();
        self::$foundRows = isset($foundRows[0]['rows']) ? (int)$foundRows[0]['rows'] : 0;

        $posts = array();
        foreach($ret as $p) {
            $posts[] = $this->getPost($p->id);
        }

        return $posts;

    }

    public function getPost($id)
    {
        $id  = (int) $id;

        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('*'));
        $select->join('slug', 'slug.post_id = post.id', array('slug'), 'left');
        $select->join('user', 'user.id = post.created_by', array('first_name'), 'left');
        $select->join('post_tag', 'post.id = post_tag.post_id', array('tags'=>new \Zend\Db\Sql\Expression('GROUP_CONCAT(post_tag.tag)')), 'left');
        $select->group('post.id');

        $w = new Where();
        $w->equalTo('post.id', $id);
        $select->where($w);

        $statement = $this->tableGateway->getSql()->prepareStatementForSqlObject($select);

        $resultSet = $statement->execute();
        $row = $resultSet->current();

        $post = new Post();
        $post->exchangeArray($row);

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT category.id, category.title, slug.slug
                  FROM post_categories
                    LEFT JOIN category  ON post_categories.category_id= category.id
                    LEFT JOIN slug ON category.id=slug.category_id
                WHERE post_categories.post_id = ?";
        $categories = $ad->query($sql, array($id));

        $post->categories = array();
        foreach($categories as $category) {
            $post->categories[] = $category;
            $post->category_title .= (empty($post->category_title) ? '' : ', ') . $category['title'];
        }

        return $post;
    }

    public function savePost(Post $post)
    {
        $data = array(
            'description' => $post->description,
            'content' => $post->content,
            'title'  => $post->title,
            'status' => $post->status,
            'published_on' => $post->published_on,
        );

        if(!empty($post->image)){
            $data['image'] = $post->image;
        }

        $id = (int) $post->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $this->tableGateway->getLastInsertValue();
            $post->id = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('post_id_seq');

        } else {
            if ($this->getPost($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Pae id does not exist');
            }
        }

        $tslug = $this->getSlugTable();
        $slug = $tslug->getSlugFor('post', $post->id);

        if(is_null($slug)) {
            $slug = new Slug();
        }

        $slug->slug = $post->slug;
        $slug->post_id = $post->id;
        $tslug->saveSlug($slug);

        $tags = explode(',', $post->tags);
        $list = "'". implode("','", $tags)."'";

        $ad = $this->tableGateway->getAdapter();
        $sql = "DELETE FROM post_tag WHERE post_id = ? AND tag NOT IN ($list)";
        $resultSet = $ad->query($sql, array($post->id));

        foreach($tags as $tag) {
            // add tag to tags table
            $sql = "INSERT IGNORE INTO tag (tag, created_on, created_by) VALUES (?, ?, ?)";
            $resultSet = $ad->query($sql, array($tag, date('Y-m-d H:i:s'), (int)LoginController::loggedUser('id')));
            // add tag to post
            $sql = "INSERT IGNORE INTO post_tag (post_id, tag) VALUES (?, ?)";
            $resultSet = $ad->query($sql, array($post->id, $tag));

        }



        return $post->id;

    }

    public function updateCategories($post_id, $category_id = array()) {

        $post_id = (int)$post_id;
        if($post_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "DELETE FROM post_categories WHERE post_id = ?";
        $resultSet = $ad->query($sql, array($post_id));

        foreach($category_id as $cid) {
            $cid = (int)$cid;
            if($cid>0) {
                $sql = "INSERT INTO post_categories (post_id, category_id) VALUES (?, ?)";
                $resultSet = $ad->query($sql, array($post_id, $cid));
            }
        }
    }

    public function getPostCategories($post_id, $selected_only = false) {

        $post_id = (int)$post_id;
        if($post_id<=0) {
            return false;
        }

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT category.id, category.title, x.post_id
                FROM category
                LEFT JOIN (SELECT * FROM post_categories WHERE post_id=?) x ON category.id=x.category_id "
            . ($selected_only ? ' AND post_id IS NOT NULL' : '');

        $resultSet = $ad->query($sql, array($post_id));

        return $resultSet;
    }

    public function deletePost($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}