<?php
/**
 * PostCategoryTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;

class PostCategoryTable extends AbstractTableGateway implements ServiceLocatorAwareInterface
{
    protected $serviceLocator;
    protected $slugTable = null;
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator() {
        return $this->serviceLocator;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select){
            $select->join('slug', 'slug.product_type_id = product_type.id', array('slug'), 'left');
            $select->order('title ASC');
        });
        return $resultSet;
    }

    public function getPostCategories($post_id, $selected_only = false) {

        $post_id = (int) $post_id;

        $resultSet = $this->tableGateway->select(function($select) use($selected_only, $post_id) {

            $predicate = new  \Zend\Db\Sql\Where();

            $select->join('post_categories', 'category.id=post_categories.category_id', array('category_id'), 'left');
            $select->where($predicate->equalTo('post_id', $post_id));
            if ($selected_only) {
                $select->where($predicate->isNull('post_id','OR'));
            }
        });

        return $resultSet;

    }

}