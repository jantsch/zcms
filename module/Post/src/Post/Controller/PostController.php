<?php
/**
 * PostController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Post\Model\Post;
use Post\Form\PostForm;
use Login\Controller\LoginController;

class PostController extends AbstractActionController
{
    protected $postTable;
    protected $config;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }
        return $this->config;
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }

    public function resizeimagesAction() {

        LoginController::checkAuthentication();


        $config = $this->getConfig();
        $list = scandir( $config['post_upload_folder']);

        foreach($list as $file) {

            $fileTypes = array('jpg', 'jpeg', 'gif', 'png'); // File extensions
            $fileParts = pathinfo($file);
            $extension = strtolower($fileParts['extension']);

            if (is_file($config['post_upload_folder'] . $file) && in_array($extension, $fileTypes)) {

                foreach ($config['post_img_sizes'] as $folder => $opts) {

                    $origin = $config['post_upload_folder'] . $file;
                    $target = $config['post_upload_folder'] . $folder . "/" . $file;

                    echo "resizing " . $target . "\r\n";
                    flush();

                    if ($folder != 'original') {
                        $w = \WideImage::load($origin);

                        if (!is_null($opts['resize'])) {
                            $w = $w->resize($opts['w'], $opts['h'], $opts['resize']);
                        }
                        if (!is_null($opts['crop'])) {

                            $w = $w->crop($opts['crop'], $opts['crop'], $opts['w'], $opts['h']);
                        }
                        $w->saveToFile($target);
                    }

                }
            }
        }

        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;


    }


    public function handleUpload($files, $title, $old_file = '')
    {
        $config = $this->getConfig();
        $name = null;
        // handle upload
        if(isset($files['image'])) {
            $file = $files['image'];

            $fileTypes = array('jpg','jpeg','gif','png'); // File extensions
            $fileParts = pathinfo($file['name']);
            if (isset($fileParts['extension']) && in_array(strtolower($fileParts['extension']),$fileTypes)) {
                if($old_file!='' && file_exists($config['post_upload_folder'] . $old_file)) {
                    @unlink($config['post_upload_folder'] . $old_file);
                    @unlink($config['post_upload_folder'] . '92x72/' . $old_file);
                    @unlink($config['post_upload_folder'] . '278x298/' . $old_file);
                    @unlink($config['post_upload_folder'] . '796x381/' . $old_file);
                }

                $mod = '';
                do {
                    $name = $this->slugify($title) . ($mod=='' ? '' : '_' .$mod) . '.' . $fileParts['extension'];
                    $target = $config['post_upload_folder'] . $name;
                    $mod++;
                }while(file_exists($target));

                $origin = $file['tmp_name'];

//                \WideImage::load($origin)->resize(92, 72, 'outside')->crop('center', 'center', 92, 72)->saveToFile($config['post_upload_folder'] . '92x72/' . $name);
//                \WideImage::load($origin)->resize(278, 298, 'outside')->crop('center', 'center', 278, 298)->saveToFile($config['post_upload_folder'] . '278x298/' . $name);
//                \WideImage::load($origin)->resize(796, 381, 'inside')->saveToFile($config['post_upload_folder'] . '796x381/' . $name);

                foreach ($config['post_img_sizes'] as $folder => $opts) {

                    $target = $config['post_upload_folder'] . $folder . "/" . $name;

                    if ($folder != 'original') {
                        $w = \WideImage::load($origin);

                        if (!is_null($opts['resize'])) {
                            $w = $w->resize($opts['w'], $opts['h'], $opts['resize']);
                        }
                        if (!is_null($opts['crop'])) {

                            $w = $w->crop($opts['crop'], $opts['crop'], $opts['w'], $opts['h']);
                        }
                        $w->saveToFile($target);
                    }

                }

                $target = $config['post_upload_folder'] . $name;
                move_uploaded_file($origin, $target);

            }
        }

        return $name;
    }


    public function indexAction()
    {
        LoginController::checkAuthentication();

        return new ViewModel(array(
            'posts' => $this->getPostTable()->fetchAll(),
        ));
    }

    public function date2bd($date) {
        $p = explode('/', $date);
        return $p[2] .'-'. $p[1] . '-' . $p[0];
    }

    public function bd2date($date) {
        return date('d/m/Y', strtotime($date));
    }

    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new PostForm();
        $form->get('published_on')->setValue(date('d/m/Y'));

        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = new Post();
            $post->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
            $form->setInputFilter($post->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $post->exchangeArray($form->getData());
                $post->image = $this->handleUpload( $request->getFiles()->toArray(), $request->getPost('title', ''));
                $post->published_on = $this->date2bd($post->published_on);
                $id = $this->getPostTable()->savePost($post);

                $categories = $request->getPost('category_id', array());
                $this->getPostTable()->updateCategories($id, $categories);

                // Redirect to list of posts
                return $this->redirect()->toRoute('post');
            }
        }
        return array(
            'form' => $form,
            'categories' => $this->getServiceLocator()->get('Category\Model\CategoryTable')->fetchAll()
        );
    }

    public function editAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('post', array(
                'action' => 'add'
            ));
        }

        try {
            $post = $this->getPostTable()->getPost($id);
            $post->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
            $post->published_on = $this->bd2date($post->published_on);
            $old_file = $post->image;
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('post', array(
                'action' => 'index'
            ));
        }

        $form  = new PostForm();
        $form->bind($post);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($post->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $post->image = $this->handleUpload( $request->getFiles()->toArray(), $request->getPost('title', ''), $old_file);
                $post->published_on = $this->date2bd($post->published_on);
                $this->getPostTable()->savePost($post);

                $categories = $request->getPost('category_id', array());
                $this->getPostTable()->updateCategories($id, $categories);

                // Redirect to list of posts
                return $this->redirect()->toRoute('post');
            }
        }

        $config = $this->getConfig();

        return array(
            'id' => $id,
            'form' => $form,
            'img_folder' => $config['post_img_folder'],
            'categories' => $this->getPostTable()->getPostCategories($id)
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('post');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {
                $id = (int) $request->getPost('id');
                $this->getPostTable()->deletePost($id);
            }

            // Redirect to list of posts
            return $this->redirect()->toRoute('post');
        }

        return array(
            'id'    => $id,
            'post' => $this->getPostTable()->getPost($id)
        );
    }

    public function getPostTable()
    {
        if (!$this->postTable) {
            $sm = $this->getServiceLocator();
            $this->postTable = $sm->get('Post\Model\PostTable');
        }
        return $this->postTable;
    }

    protected $postCategoryTable;
    public function getPostCategoryTable()
    {

        if (!$this->postCategoryTable) {
            $sm = $this->getServiceLocator();
            $this->postCategoryTable = $sm->get('Post\Model\PostCategoryTable');
        }
        return $this->postCategoryTable;
    }
}