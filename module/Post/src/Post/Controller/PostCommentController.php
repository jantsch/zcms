<?php
/**
 * PostController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Post\Form\PostCommentForm;
use Login\Controller\LoginController;
use Zend\Session\Container;

class PostCommentController extends AbstractActionController
{
    protected $postCommentTable;
    protected $config;

    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getServiceLocator()->get('Config');
        }
        return $this->config;
    }

    public function getPostCommentTable()
    {
        if (!$this->postCommentTable) {
            $sm = $this->getServiceLocator();
            $this->postCommentTable = $sm->get('Post\Model\PostCommentTable');
        }
        return $this->postCommentTable;
    }
    public function indexAction()
    {
        LoginController::checkAuthentication();

        $session = new Container('zcms');
        $filter = '';
        $request = $this->getRequest();
        if($request->isPost()) {
            $filter = $request->getPost('status-filter', '');

        }

        if($filter !== '') {
            $session->comments_filter = $filter;

        }else {
            if ($session->offsetExists('comments_filter')) {
                $filter = $session->comments_filter;

            } else {
                $filter = 'review';

            }
        }

        return new ViewModel(array(
            'comments' => $this->getPostCommentTable()->getByStatus($filter),
            'filter' => $filter
        ));
    }

    public function date2bd($date) {
        $p = explode('/', $date);
        return $p[2] .'-'. $p[1] . '-' . $p[0];
    }

    public function bd2date($date) {
        return date('d/m/Y', strtotime($date));
    }

    public function approveAction() {

        LoginController::checkAuthentication();

        $json = array('error' => true);
        $id = (int) $this->params()->fromRoute('id', 0);;
        if ($id) {
            try {
                $postComment = $this->getPostCommentTable()->getPostComment($id);
                $postComment->status = 'approved';
                $this->getPostCommentTable()->savePostComment($postComment);
                $json['error'] = false;
            }
            catch (\Exception $ex) {}
        }
        $this->layout('layout/json');
        return array('data' => $json);
    }


    public function rejectAction() {

        LoginController::checkAuthentication();

        $json = array('error'=>true);
        $id = (int) $this->params()->fromRoute('id', 0);;
        if ($id) {
            try {
                $postComment = $this->getPostCommentTable()->getPostComment($id);
                $postComment->status='rejected';
                $this->getPostCommentTable()->savePostComment($postComment);
                $json['error']=false;
            }
            catch (\Exception $ex) {}
        }
        $this->layout('layout/json');
        return array('data' => $json);
    }


    public function editAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);;
        if (!$id) {
            return $this->redirect()->toRoute('post-comment', array('action' => 'add'));
        }

        try {
            $postComment = $this->getPostCommentTable()->getPostComment($id);
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('post-comment', array('action' => 'index'));
        }

        $form  = new PostCommentForm();
        $form->bind($postComment);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($postComment->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getPostCommentTable()->savePostComment($postComment);
                return $this->redirect()->toRoute('post-comment');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('post-comment');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'Cancelar');

            if ($del == 'Remover') {
                $id = (int) $request->getPost('id');
                $this->getPostCommentTable()->deletePostComment($id);
            }

            return $this->redirect()->toRoute('post-comment');
        }

        return array(
            'id'    => $id,
            'comment' => $this->getPostCommentTable()->getPostComment($id)
        );
    }

}