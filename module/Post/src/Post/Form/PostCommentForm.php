<?php
/**
 * PostForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Form;

use Zend\Form\Form;

class PostCommentForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('post');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'post_id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'subject',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'message',
            'type' => 'Textarea',
        ));



        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    'review' => 'revisar',
                    'approved' => 'aprovado',
                    'rejected' => 'rejeitado'
                )
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}