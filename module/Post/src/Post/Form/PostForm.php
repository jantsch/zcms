<?php
/**
 * PostForm.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Post\Form;

use Zend\Form\Form;

class PostForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct('post');

        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'title',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'description',
            'type' => 'Textarea',
        ));

        $this->add(array(
            'name' => 'slug',
            'type' => 'Text',
            'options' => array(
                'label' => 'Slug',
            ),
        ));

        $this->add(array(
            'name' => 'tags',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'slug',
            'type' => 'Text',
        ));


        $this->add(array(
            'name' => 'published_on',
            'type' => 'Text'
        ));

        $this->add(array(
            'name' => 'status',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    'revision' => 'revisao',
                    'draft' => 'rascunho',
                    'published' => 'publicado'
                )
            )
        ));

        $this->add(array(
            'name' => 'content',
            'type' => 'Text',
        ));

        $this->add(array(
            'name' => 'image',
            'type' => 'File',
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Salvar',
                'id' => 'submitbutton',
            ),
        ));
    }
}