<?php
/**
 * ConfigTable.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Application\Model;

use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class ConfigTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select(function($select){
            $select->order('property ASC');
        });

        return $rowset;
    }

    public function getConfig($property)
    {
        $where = new Where();
        $single = true;
        if(is_array($property)) {
            $where->in('property', $property);
            $single = false;
        }else{
            $where->equalTo('property', $property);
        }

        $rowset = $this->tableGateway->select($where);

        return ($single ? $rowset->current() : $rowset);

    }

    /**
     * Add one property at time
     * @param Config $config
     */
    public function saveConfig($property, $value)
    {
        $property = trim(filter_var($property, FILTER_SANITIZE_STRING));
        $value = trim(filter_var($value, FILTER_SANITIZE_STRING));

        if(!empty($property) && !empty($value)) {
            $data = array(
                'property' => $property,
                'value' => $value
            );
            $this->tableGateway->insert($data);
        }
    }

    /**
     * Empty config table
     * @param $id
     */
    public function deleteConfig()
    {
        $where = new Where();
        $where->notIn('property', array(''));
        $this->tableGateway->delete($where);
    }
}