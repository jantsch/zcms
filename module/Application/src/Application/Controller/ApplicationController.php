<?php
/**
 * Main application controler - configuration manager
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Login\Controller\LoginController;

class ApplicationController extends AbstractActionController
{
    protected $configTable;

    public function getConfigTable()
    {
        if (!$this->configTable) {
            $sm = $this->getServiceLocator();
            $this->configTable = $sm->get('Application\Model\ConfigTable');
        }
        return $this->configTable;
    }
    public function configAction()
    {
        LoginController::checkAuthentication();

        $properties = $this->getConfigTable()->fetchAll()   ;

        return array('properties' => $properties);
    }

    public function saveconfigAction()
    {
        LoginController::checkAuthentication();

        $request = $this->getRequest();

        $prop = $request->getPost('property',array());
        $val = $request->getPost('value',array());

        if( count($prop)==count($val)) {


            $tbl = $this->getConfigTable();
            $tbl->deleteConfig();
            foreach($prop as $i => $v) {
                $tbl->saveConfig( $prop[$i], $val[$i]);
            }

        }

        $this->redirect()->toRoute('admin');

    }
}
