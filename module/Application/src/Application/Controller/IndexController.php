<?php
/**
* IndexController.php
* @author Gustavo Jantsch <jantsch@gmail.com>
* @copyright Copyright (c) 2016 Gustavo Jantsch
*/

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Login\Controller\LoginController;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        LoginController::checkAuthentication();

        $vm = new ViewModel();
        $vm->setTemplate('layout/admin_home.phtml');
        return $vm;

    }

    public function adminAction()
    {

        LoginController::checkAuthentication();

        $vm = new ViewModel();
        $vm->setTemplate('layout/admin_home.phtml');
        return $vm;

    }


    public function configAction()
    {
        LoginController::checkAuthentication();
    }
}
