<?php
/**
 * module.config.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Application;

return array(
    'router' => array(
        'routes' => array(
            // essa rota � a home do site
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Site',
                        'action'     => 'index',
                    ),
                ),
                'child_routes' =>array(
                    'posts' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route' => '[:slug]',
                            'constraints' => array(
                                'slug' => '[a-zA-Z0-9_-]+'
                            ),
                            'defaults' => array(
                                '__NAMESPACE__' => 'Site\Controller',
                                'controller'    => 'Site',
                                'action'        => 'view',

                            ),
                        ),
                    ),
                )
            ),
            // essas s�o as homes do admin
            'admin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'admin',
                    ),
                ),
            ),
            'admin1' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'admin',
                    ),
                ),
            ),
            'config' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin/application/config',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Application',
                        'action'     => 'config',
                    ),
                ),
            ),
            'saveconfig' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/admin/application/saveconfig',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Application',
                        'action'     => 'saveconfig',
                    ),
                ),
            ),
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/admin/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\IndexController',
                                'action'     => 'index'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'factories' => array(
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ),
    ),
    'translator' => array(
        'locale' => 'pt_BR',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Application' => 'Application\Controller\ApplicationController',

        ),
    ),
    'view_manager' => array(
        'doctype'                  => 'HTML5',
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'         => __DIR__ . '/../view/layout/layout.phtml',
            'layout/popup'         => __DIR__ . '/../view/layout/popup.phtml',
            'layout/emtpy'         => __DIR__ . '/../view/layout/emtpy.phtml',
            'layout/json'           => __DIR__ . '/../view/layout/json.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
//            'site/layout'           => __DIR__ . '/../view/site/index.phtml',
//            'error/404'               => __DIR__ . '/../view/site/404.phtml',
//            'error/index'             => __DIR__ . '/../view/site/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
            'application' => __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
