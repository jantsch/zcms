<?php
/**
 * Module.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Application;

use Zend\Validator\AbstractValidator;
use Zend\Mvc\I18n\Translator;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Session\Container;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\Config;
use Application\Model\ConfigTable;

class Module
{


    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $translator = $e->getApplication ()->getServiceManager ()->get ( 'translator' );
        $translator->addTranslationFile ( 'phpArray', './vendor/zendframework/zend-i18n-resources/languages/pt_BR/Zend_Validate.php' );
        $translator->setLocale ( 'pt_BR' );
        AbstractValidator::setDefaultTranslator ( new Translator ( $translator ) );

        // jntx - send session data to layout
        $session = new Container('zcms');
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->zcms_session = $session->getArrayCopy();
        $viewModel->app_config = $e->getApplication()->getServiceManager()->get('Config');

    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Application\Model\ConfigTable' => function ($sm) {
                    $tableGateway = $sm->get('ConfigTableGateway');
                    $table = new ConfigTable($tableGateway);
                    return $table;
                },
                'ConfigTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Config());
                    return new TableGateway('config', $dbAdapter, null, $resultSetPrototype);
                }
            ),
        );
    }
}
