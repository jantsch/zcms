<?php

/**
 * LoginController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Login\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Login\Form\LoginForm;
use Zend\Session\Container;

class LoginController extends AbstractActionController
{
    protected $userTable;
    protected $loggedUser;

    public static function loggedUser($field=false)
    {
        $session = new Container('zcms');

        if($field != false && isset($session->logged_user[$field])) {
            return $session->logged_user[$field];

        }
        return $session->logged_user;
    }


    public static function checkAuthentication()
    {
        $session = new Container('zcms');

        if($session->offsetExists('logged_user') && (int)$session->logged_user['id'] > 0 ) {
            return true;

        }else{
            header("Location: /login");
            exit;

        }
    }

    public function indexAction()
    {
        $this->layout('layout/login');

        return new ViewModel(array(
            'users' => $this->getUserTable()->fetchAll(),
        ));
    }

    public function validateAction()
    {
        $form  = new LoginForm();

        $request = $this->getRequest();
        if ($request->isPost()) {

            try {
                $email = $request->getPost('email', null);
                $pwd = $request->getPost('password', null);
                $user =  $this->getUserTable()->authenticateUser($email, $pwd);
                $user->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));

                if($user->id > 0 || true) {

                    $session = new Container('zcms');
                    $session->logged_user = array(
                        'name' => $user->first_name . " " . $user->last_name,
                        'id' => $user->id,
                        'email' => $user->email,
                        'is_admin' => $user->is_admin
                    );
                    return $this->redirect()->toRoute('user');
                }
            }
            catch (\Exception $ex) {
                return $this->redirect()->toRoute('login', array(
                    'action' => 'index'
                ));
            }


            $form->setInputFilter($user->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getUserTable()->saveUser($user);

                // Redirect to the list of users
                return $this->redirect()->toRoute('user');
            }
        }

        return $this->redirect()->toRoute('login');
    }

    public function exitAction() {

        $session = new Container('zcms');
        $session->offsetUnset('logged_user');
        return $this->redirect()->toRoute('login');
    }

    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }


}