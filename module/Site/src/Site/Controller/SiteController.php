<?php
/**
 * SiteController.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Site\Controller;

use Post\Model\PostCommentTable;
use Post\Model\PostComment;
use Post\Model\PostTable;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Model\Config;
use Application\Model\ConfigTable;
use Banner\Model\Banner;
use Banner\Model\BannerTable;
use Product\Model\ProductTypeTable;
use Zend\Mail;

class SiteController extends AbstractActionController
{
    protected $siteTable;
    protected $configTable;
    protected $config;

    protected $postCommentTable;
    protected $postTable;
    protected $pageTable;
    protected $productTable;
    protected $productTypeTable;
    protected $procatTable;
    protected $protypeTable;
    protected $categoryTable;
    protected $galleryTable;

    protected $tables;

    public function getTable($table) {
        if(is_null($this->tables)) {
            $this->tables = array();
        }
        if(!isset($this->tables[$table])) {
            $sm = $this->getServiceLocator();
            $this->tables[$table] = $sm->get($table);
        }
        return $this->tables[$table];
    }

    public function getPostCommentTable() {
        return $this->getTable('Post\Model\PostCommentTable');
    }

    public function getPostTable() {
        return $this->getTable('Post\Model\PostTable');
    }
      
    public function getPageTable() {
        return $this->getTable('Page\Model\PageTable');
    }
    
    public function getProductTable() {
        return $this->getTable('Product\Model\ProductTable');
    }
        
    public function getProductTypeTable() {
        return $this->getTable('Product\Model\ProductTypeTable');
    }

    public function getProcatTable() {
        return $this->getTable('Product\Model\ProcatTable');
    }

    public function getProtypeTable() {
        return $this->getTable('Product\Model\ProductTypeTable');
    }

    public function getCategoryTable() {
        return $this->getTable('Category\Model\CategoryTable');
    }

    public function getGalleryTable() {
        return $this->getTable('Gallery\Model\GalleryTable');
    }

    public function getBannerTable() {
        return $this->getTable('Banner\Model\BannerTable');
    }


    /**
     * Get config from db config table
     */
    public function getConfig($param=null)
    {
        if (!$this->configTable) {
            $sm = $this->getServiceLocator();
            $this->configTable = $sm->get('Application\Model\ConfigTable');
        }

        if(!$this->config) {
            $vars = $this->configTable->fetchAll();
            $this->config = array();
            foreach ($vars as $conf) {
                $this->config[$conf->property] = $conf->value;
            }
        }

        if(is_string($param)) {
            return isset($this->config[$param]) ? $this->config[$param] : null;
        }

        return $this->config;

    }

    /**
     * Set globals to layout
     *
     * @param ViewModel $view
     */
    public function setGlobals($view) {

        // global config vars
        $this->getConfig();
        $cfg = $this->config;

        // now get the global configuration properties
        $config = $this->getServiceLocator()->get('Config');
        foreach($config as $k => $v) {
            $cfg[$k] = $v;
        }

        $menu_produtos = $this->getProductTable()->fetchAll();
        $linha_de_produtos = $this->getProtypeTable()->fetchAll();
        $categoria_posts = $this->getCategoryTable()->fetchAll()->toArray();
        $banner_top = $this->getBannerTable()->getTopBanner();

        $this->layout()->setVariable('app_properties', $cfg);
        $this->layout()->setVariable('menu_produtos', $menu_produtos);
        $this->layout()->setVariable('linha_de_produtos', $linha_de_produtos);
        $this->layout()->setVariable('categoria_posts', $categoria_posts);
        $this->layout()->setVariable('banner_top', $banner_top);
        $view->menu_produtos = $menu_produtos;
        $view->linha_de_produtos = $linha_de_produtos;
        $view->categoria_posts = $categoria_posts;
        $view->app_properties = $cfg;


    }

    public function showHomePage()
    {

        $bloco = array(
            1 => $this->getPageTable()->getPageBySlug('home-bloco-01'),
            2 => $this->getPageTable()->getPageBySlug('home-bloco-02'),
            3 => $this->getPageTable()->getPageBySlug('home-bloco-03'),
            4 => $this->getPageTable()->getPageBySlug('home-bloco-04'),
        );

        $home = new ViewModel(array(
            'banners' => $this->getBannerTable()->getActiveBanners(),
            'posts' => $this->getPostTable()->getMostRecent(0, 2)->toArray(),
            'dicas' => $this->getCategoryTable()->fetchAll(),
            'linhas' => $this->getProductTypeTable()->fetchAll(),
            'bloco' => $bloco
        ));
        $home->setTemplate('site/home.phtml');
        $this->setGlobals($home);

        return $home;

    }

    public function getProductPage($pro) {

        $pgTable = $this->getProductTable();
        $images = $pgTable->getImages($pro->id);
        $produtosLinha = $this->getProductTable()->fetchProductsByType($pro->type_id);

        // remove o produto sendo exibido dos relacionados
        $relacionados = $produtosLinha;
        foreach($relacionados as $index => &$row) {
            if($row->id == $pro->id) {
                unset($relacionados[$index]);
            }
        }
        unset($row);

        $rel_categorias = $this->getProductTable()->getRelatedIds($pro->id);
        if(!empty($rel_categorias)) {
            $adicionar = $pgTable->fetchAll(explode(',', $rel_categorias));
            foreach($adicionar as $row) {
                if($row->type_id != $pro->type_id) {
                    array_unshift($relacionados, $row);
                }
            }
        }

        $vm = new ViewModel(array(
                'produto' => $pro,
                'linha' => $this->getProductTypeTable()->getProductType($pro->type_id),
                'produtos_linha' => $produtosLinha,
                'produtos_relacionados' => $relacionados,
                'images' => $images
            )
        );
        $vm->setTemplate('site/produto.phtml');

        return $vm;

    }

    /**
     * From slug string, get appropriated content
     *
     * @param $slug
     * @return array
     */
    public function getSlug($slug)
    {

        $type = null;
        $obj = null;
        $id = null;

        $slugRow =  $this->getTable('Slug\Model\SlugTable')->getSlug($slug);

        if($slugRow) {
            if($slugRow->product_type_id>0) {
                $type = 'type';
                $id = $slugRow->product_type_id;

            }elseif($slugRow->category_id>0) {
                $type = 'category';
                $id = $slugRow->category_id;

            }elseif($slugRow->page_id>0) {
                $obj = $this->getPageTable()->getPage($slugRow->page_id);
                $type = 'page';
                $id = $slugRow->page_id;

            }elseif($slugRow->post_id>0) {
                $type = 'post';
                $id = $slugRow->post_id;

            }elseif($slugRow->product_id>0) {
                $obj = $this->getProductTable()->getProduct($slugRow->product_id);
                $type = 'product';
                $id = $slugRow->product_id;

            }
        }

        return array('type' => $type, 'obj' =>$obj, 'id' => $id);

    }

    public function shortCode_dicas_e_truques() {

        $dicas = $this->getPostTable()->getByTag('dicas', 0, 6);
        ob_start();
        include __DIR__ . "/../../../view/site/dicas_e_truques.phtml";
        return ob_get_clean();

    }

    public function viewAction(){

        $this->layout('site/index');

        $slug = $this->params()->fromRoute('slug', null);
        $page = (int)$this->getRequest()->getQuery()->offsetGet('page');

        if($slug!==null) {

            if($slug=='blog') {
                $publicados = $this->getPostTable()->getPublished($page, 3);
                $publicados_total = PostTable::$foundRows;
                $publicados_paginas = ceil($publicados_total / 3);
                $topicos_recentes = $this->getPostTable()->getMostRecent(0, 3);
                $galerias = $this->getGalleryTable()->fetchPublic();

                $vm = new ViewModel(array(
                        'posts' => $publicados,
                        'total_posts' => $publicados_total,
                        'posts_recentes' => $topicos_recentes->toArray(),
                        'posts_paginas' => $publicados_paginas,
                        'galerias' => $galerias,
                        'dicas' => $this->getPostTable()->getByTag('dicas', 0, 6),
                        'data' => array()

                    )
                );
                $vm->setTemplate('site/blog.phtml');


            }elseif($slug=='search') {

                $q = $this->getRequest()->getQuery()->offsetGet('q');
                $q = str_replace(array(' ', '-'), '%', trim($q));
                $type = $this->getRequest()->getQuery()->offsetGet('type');
                if($type=='product') {
                    $results = $this->getProductTable()->getSearch($q,$page, 10);
                }else{
                    $results = $this->getPostTable()->getSearch($q, $page, 10);
                }

                $vm = new ViewModel(array(
                        'results' => $results,
                        'term' => $q,
                        'page' => $page,
                        'total_paginas' => ceil(PostTable::$foundRows/10)
                    )
                );
                $vm->setTemplate('site/search.phtml');


            }elseif($slug=='galeria') {

                $id = (int)$this->getRequest()->getQuery()->offsetGet('id');
                $vm = new ViewModel(array(
                        'galeria' => $this->getGalleryTable()->getGallery($id),
                        'imagens' => $this->getGalleryTable()->getGalleryImages($id),
                        'galerias' => $this->getGalleryTable()->fetchPublic(),

                    )
                );
                $vm->setTemplate('site/gallery.phtml');

            }elseif($slug=='trabalhe-conosco') {
                $sent = false;
                $msg = '';
                $request = $this->getRequest();

                if($request->getPost('enviar')=='true' && isset($_SESSION['form-key']) ) {

                    $msg = array();
                    $msg[] = 'Nome: '.  $request->getPost('nome');
                    $msg[] = 'Endereço: '.  $request->getPost('endereco');
                    $msg[] = 'Telefone: '. $request->getPost('telefone');
                    $msg[] = 'Email: ' . $request->getPost('email');
                    $msg[] = 'Nascimento: '. $request->getPost('nascimento');
                    $msg[] = 'Escolaridade: ' . $request->getPost('escolaridade');
                    $msg[] = '<hr />';
                    $msg[] = 'Experiência';
                    $msg[] = $request->getPost('experiencia');
                    $msg[] = '<hr />';
                    $msg[] = 'Qualificacoes';
                    $msg[] = $request->getPost('qualificacoes');
                    $msg[] = '<hr />';
                    $msg[] = 'Cursos';
                    $msg[] = $request->getPost('cursos');
                    $msg[] = '<hr />';
                    $msg[] = 'Area';
                    $msg[] = $request->getPost('area');
                    $msg[] = '<hr />';
                    $msg[] = 'Objetivo';
                    $msg[] = $request->getPost('objetivo');
                    $msg[] = '<hr />';
                    $msg[] = 'Receber novidades: ' . ($request->getPost('novidades')=='on' ? ' sim ' : ' nao ');

                    foreach($msg as &$val) {
                        $val = trim(filter_var($val, FILTER_SANITIZE_STRING));
                    }

                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $sent = true;
                    $body = implode("<br />\r\n\r\n", $msg);
                    if( mail($this->getConfig('email-trabalhe-conosco'), 'Curriculo Site', $body, $headers) ) {
                        $msg = 'Seu currículo foi enviado com sucesso.';
                    }else{
                        $msg = 'Ocorreu um erro ao tentar enviar seu currículo, tente novamente mais tarde.';
                    }
                }

                $_SESSION['form-key'] = time();

                $vm  = new ViewModel(array(
                    'enviado' => $sent,
                    'mensagem' => $msg
                ));
                $vm->setTemplate(('site/trabalhe-conosco.phtml'));


            }else {
                $obj = $this->getSlug($slug);

                switch ($obj['type']) {
                    case 'page':

                        $short_codes = array();
                        $count = preg_match_all('/\[secao: (.*)\]/im', $obj['obj']->content, $short_codes);
                        if(is_array($short_codes) && count($short_codes)>0) {
                            foreach($short_codes[1] as $code) {
                                $method = "shortCode_{$code}";
                                if(method_exists($this,$method)) {
                                    $obj['obj']->content = str_replace("[secao: $code]", $this->$method(), $obj['obj']->content);
                                }
                            }
                        }

                        $vm = new ViewModel(array('page' => $obj['obj']));
                        $vm->setTemplate('site/page.phtml');
                        break;

                    case 'category':

                        $page = (int)$this->getRequest()->getQuery()->offsetGet('page');
                        $publicados = $this->getPostTable()->getPostsByCategory($obj['id'], $page, 3);
                        $publicados_total = PostTable::$foundRows;
                        $publicados_paginas = ceil($publicados_total / 3);
                        $topicos_recentes = $this->getPostTable()->getMostRecent(0, 3);
                        $galerias = $this->getGalleryTable()->fetchPublic();

                        $categoria = $this->getCategoryTable()->getCategory($obj['id']);

                        $vm = new ViewModel(array(
                                'categoria' =>$categoria,
                                'posts' => $publicados,
                                'total_posts' => $publicados_total,
                                'posts_recentes' => $topicos_recentes->toArray(),
                                'posts_paginas' => $publicados_paginas,
                                'galerias' => $galerias,
                                'dicas' => $this->getPostTable()->getByTag('dicas', 0, 6),
                                'data' => array()
                            )
                        );
                        $vm->setTemplate('site/blog.phtml');

                        break;

                    case 'post':

                        $post = $this->getPostTable()->getPost($obj['id']);
                        $topicos_recentes = $this->getPostTable()->getMostRecent(0, 3);
                        $galerias = $this->getGalleryTable()->fetchPublic();

                        $vm = new ViewModel(array(
                            'post' => $post,
                            'posts_recentes' => $topicos_recentes->toArray(),
                            'galerias' => $galerias,
                            'dicas' => $this->getPostTable()->getByTag('dicas', 0, 6),
                            'comments' => $this->getPostCommentTable()->getByStatus('approved', $obj['id']),

                        ));

                        $vm->setTemplate('site/post.phtml');
                        break;

                    case 'type':
                        // LINHA DE PRODUTO : mostra primeiro produto
                        $products =  $this->getProductTable()->fetchProductsByType($obj['id']);
                        $product = isset($products[0]) ? $products[0] : false;
                        $obj['obj'] = (!$product ? false : $this->getProductTable()->getProduct($product->id));
                        // no break

                    case 'product':
                        if(!$obj['obj']) {
                            $vm = new ViewModel();
                            $vm->setTemplate('site/404.phtml');
                        }else {
                            $vm = $this->getProductPage($obj['obj']);
                        }
                        break;

                    default:
                        $vm = new ViewModel();
                        $vm->setTemplate('site/404.phtml');
                }
            }

        }else{
            $vm = $this->showHomePage();
        }

        $this->layout()->setVariable('slug_atual', $slug);
        $vm->slug_atual = $slug;
        $this->setGlobals($vm);
        return $vm;

    }

    public function quickcontactAction()
    {
        $request = $this->getRequest();
        $config = $this->getConfig();

        $msg = 'Nome: ' . $request->getPost('quick-name') . "\r\n" .
            'Telefone' . $request->getPost('quick-phone') . "\r\n" .
            'Email' . $request->getPost('quick-email ') . "\r\n" .
            $request->getPost('quick-message ');

        $mail = new Mail\Message();
        $mail->setBody($msg);
        $mail->setFrom($config['email-contato'], 'Contato do site');
        $mail->addTo($config['email-contato'], 'Contato do site');
        $mail->setSubject('Contato do site');

        $transport = new Mail\Transport\Sendmail();
        $transport->send($mail);

        $this->layout('site/json');
        return array('content' => json_encode(array('sent' => true)));


    }

    public function newsletterSignupAction()
    {
        $request = $this->getRequest();
        $config = $this->getConfig();

        $msg = 'Nome: ' . $request->getPost('nome') . "\r\n" .
            'Email' . $request->getPost('email') . "\r\n";

        $mail = new Mail\Message();
        $mail->setBody($msg);
        $mail->setFrom($config['email-cadastro-newsletter'], 'Contato do site');
        $mail->addTo($config['email-cadastro-newsletter'], 'Contato do site');
        $mail->setSubject('Cadastro da newsletter');

        $transport = new Mail\Transport\Sendmail();
        $transport->send($mail);

        $this->layout('site/json');
        return array('content' => json_encode(array('error' => false, 'msg' => 'Sua assinatura foi realizada com sucesso.')));

    }


    public function contactAction()
    {
        $request = $this->getRequest();
        $config = $this->getConfig();

        $msg = '';
        $data = $request->getPost();
        foreach($data as $f => $v ){
            $f = str_replace('_', ' ', strip_tags($f));
            $msg .= $f . ": " . strip_tags($v) . "\r\n";
        }

        $mail = new Mail\Message();
        $mail->setBody($msg);
        $mail->setFrom($config['email-contato'], 'Contato do site');
        $mail->addTo($config['email-contato'], 'Contato do site');
        $mail->setSubject('Contato do site');

        $transport = new Mail\Transport\Sendmail();
        $transport->send($mail);

        $this->layout('site/json');
        return array('content' => json_encode(array('sent' => true, 'content'=> $msg)));

    }

    public function postCommentAction()
    {

        $request = $this->getRequest();

        $comment = new PostComment();
        $comment->post_id = $request->getPost('post_id', '');
        $comment->name = $request->getPost('name', '');
        $comment->email = $request->getPost('email', '');
        $comment->subject = $request->getPost('subject', '');
        $comment->message = $request->getPost('message', '');
        $comment->status = 'review';

        $id = $this->getPostCommentTable()->savePostComment($comment);

        $error = ($id<=0);
        $msg = ($error ? 'Não foi possível salvar seu comentário, tente mais tarde.' : 'Agradecemos seu comentário, em breve estará visível junto com o post.');

        $this->layout('site/json');
        return array('content' => json_encode(array('error' => $error, 'msg'=> $msg)));

    }

}