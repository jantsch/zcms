<?php
/**
 * module.config.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

return array(
    'controllers' => array(
        'invokables' => array(
            'Site\Controller\Site' => 'Site\Controller\SiteController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'site' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/site[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Site\Controller\Site',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_path_stack' => array(
            'site' => __DIR__ . '/../view',
            'error/404'               => __DIR__ . '/../view/site/404.phtml',
            'error/index'             => __DIR__ . '/../view/site/index.phtml',
            'site/index'             => __DIR__ . '/../view/site/index.phtml',
            'site/json'             => __DIR__ . '/../view/site/json.phtml',

        ),
    ),
);