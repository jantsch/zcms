<!-- Page Banner -->
<div class="page-banner" id="page-banner"><img alt="page-banner" src="/images/page-banner.png" /> <!-- container -->
    <div class="page-detail">
        <div class="container">
            <h3 class="page-title">Endere&ccedil;os</h3>

            <div class="page-breadcrumb pull-right">
                <ol class="breadcrumb">
                    <li><a href="/" title="Home">Home</a></li>
                    <li><a href="/contato" title="Contato">Contato</a></li>
                    <li>Endere&ccedil;os</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- container /- --></div>
<!-- Page Banner /- --><!-- contact Details -->

<div class="contact-detail" id="contact-detail"><!-- container -->
    <div class="container">
        <div class="col-md-6 col-sm-6 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/address-icon.png" />
            <h3>MATRIZ</h3>

            <p>Estrada de Adrian&oacute;polis, 2700 - Galp&atilde;o - Parte</p>

            <p>Bairro Santa Rita - Nova Igua&ccedil;u/RJ - CEP: 26042-660</p>

            <p>Telefone: (21) 2765-9559</p>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/customer-service-icon.png" />
            <h3>Televendas</h3>

            <p>(21) 2765-9550 Ramal: 177 / 244 / 247</p>
            &nbsp;

            <p>&nbsp;</p>

            <p>&nbsp;</p>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/address-icon.png" />
            <h3>FILIAL MG</h3>

            <p>Rua Continental, n&deg; 300, sala 10,</p>

            <p>Cinc&atilde;o, Contagem, MG - CEP: 32.371-620</p>

            <p>Telefone: (31) 3390-2832</p>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/address-icon.png" />
            <h3>FILIAL PE</h3>

            <p>Rodovia BR 101 Sul, KM 82,7, S/N, sala I 4B,</p>

            <p>Prazeres, Jaboat&atilde;o dos Guararapes, PE, CEP: 54.335-000</p>

            <p>Telefone: (81) 3476-1374</p>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/address-icon.png" />
            <h3>FILIAL BA</h3>

            <p>Av Aliomar Baleeiro, 15493, loja 18,</p>

            <p>S&atilde;o Cristov&atilde;o, Salvador, BA, CEP: 41.500-660</p>

            <p>Telefone: (71) 3365-7015</p>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/address-icon.png" />
            <h3>FILIAL SP</h3>

            <p>Rua Arinos, 155, sala 07,</p>

            <p>Ind&uacute;stria Anhanguera , Osasco, SP - CEP: 06.276-032</p>

            <p>Telefone:(16) 99226-8344 / (21) 96721-0045</p>
        </div>

        <div class="col-md-4 col-sm-4 col-xs-4 contact-detail-box"><img alt="Address" src="/images/icon/address-icon.png" />
            <h3>FILIAL ES</h3>

            <p>Rua Marataizes, 250, sala 407,</p>

            <p>Valparaiso, Serra, ES, CEP: 29.165-827</p>

            <p>Telefone:(27)99834-6095 / (27)99810-4585</p>
        </div>
    </div>
    <!-- container /- --></div>
<!-- contact Details --><!-- Contact Form -->

<div class="contact-form-section ow-section" id="contact"><!-- container -->
    <div class="container">
        <div class="row">
            <h3>Deixe sua mensagem aqui</h3>

            <form class="main-contact-form" id="main-contact-form" method="post">
                <div class="col-md-6 col-sm-6"><label>Assunto <span>*</span></label> <select name="contact-subject"><option value="Sugest�o">Sugest&atilde;o</option><option value="Reclama��o">Reclama&ccedil;&atilde;o</option><option value="D�vida">D&uacute;vida</option><option value="Informa��es">Informa&ccedil;&otilde;es</option><option value="Elogio">Elogio</option><option value="Outros">Outros</option> </select></div>

                <div class="col-md-6 col-sm-6"><label>Nome <span>*</span></label> <input id="input_name" name="contact-name" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>E-mail <span>*</span></label> <input id="input_email" name="contact-email" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Telefone <span>*</span></label> <input id="input_phone" name="contact-phone" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Endere&ccedil;o</label> <input id="input_street" name="contact-street" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Complemento</label> <input id="input_category" name="contact-category" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Cidade</label> <input id="input_city" name="contact-city" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Estado</label> <input id="input_state" name="contact-state" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>CEP</label> <input id="input_zipcode" name="contact-zipcode" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Nome do Produto</label> <input name="contact-product" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Data de Fabrica&ccedil;&atilde;o (Impressa na Embalagem)</label> <input name="contact-date" type="text" /></div>

                <div class="col-md-6 col-sm-6"><label>Lote (Impressa na Embalagem)</label> <input name="contact-set" type="text" /></div>

                <div class="col-md-12 col-sm-12"><label>Mensagem</label><textarea id="textarea_message" name="contact-message"></textarea></div>

                <div class="col-md-12 col-sm-12">
                    <input name="contact-grhigiene" type="checkbox" value="Sim, receber newsletter GR Higiene e Limpeza" style="display: inline !important; width: auto; height: auto;" />&nbsp;<label style="display: inline !important; width: auto;">Desejo receber novidades e informa&ccedil;&otilde;es sobre a GR Higiene e Limpeza por email.</label>
                    &nbsp;
                </div>
                
            </form>
        </div>

        <div class="col-md-12 col-sm-12"><input class="btn" id="btn_smt" type="button" value="Enviar" /></div>

        <div class="col-md-12 col-sm-12">
            <div class="alert-msg" id="alert-msg">&nbsp;</div>
        </div>
    </div>
</div>
<!-- container /- --><!-- Contact Form /- -->