<?php
/**
 * Module.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Site;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc;
use Zend\Mvc\Application;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    public function init(ModuleManager $moduleManager) {
        $moduleManager->getEventManager()->getSharedManager()->attach(__NAMESPACE__, 'dispatch', function($event){
        });

    }

    public function onBootstrap(\Zend\Mvc\MvcEvent $event)
    {
        $app = $event->getParam( 'application' );
        $eventManager = $app->getEventManager();
        $eventManager->attach( \Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, function( $event ){
            $serviceManager = $event->getApplication()->getServiceManager();
        });

    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array();
    }

    public function handleControllerNotFoundAndControllerInvalidAndRouteNotFound($e)
    {
        $error  = $e->getError();
        if ($error == Application::ERROR_CONTROLLER_NOT_FOUND) {
            $logText =  'The requested controller '
                .$e->getRouteMatch()->getParam('controller'). '  could not be mapped to an existing controller class.';
            echo $logText;
        }

        if ($error == Application::ERROR_CONTROLLER_INVALID) {
            //the controller doesn't extends AbstractActionController
            $logText =  'The requested controller '
                .$e->getRouteMatch()->getParam('controller'). ' is not dispatchable';
            echo $logText;
        }

        if ($error == Application::ERROR_ROUTER_NO_MATCH) {
            // the url doesn't match route, for example, there is no /foo literal of route
            $logText =  'The requested URL could not be matched by routing.';
            echo $logText;
        }
    }
}