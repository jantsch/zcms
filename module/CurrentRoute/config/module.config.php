<?php
/**
 * module.config.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

return array(
    'view_helpers' => array(
        'factories' => array(
            'currentRoute' => '\CurrentRoute\Factory\View\Helper\CurrentRouteFactory'
        )
    )
);
