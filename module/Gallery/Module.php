<?php
/**
 * Module.php
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Gallery;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Gallery\Model\Gallery;
use Gallery\Model\GalleryTable;
use Gallery\Model\GalleryImage;
use Gallery\Model\GalleryImageTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\ModuleManager;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface
{

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Gallery\Model\GalleryTable' =>  function($sm) {
                    $tableGateway = $sm->get('GalleryTableGateway');
                    $table = new GalleryTable($tableGateway);
                    return $table;
                },
                'GalleryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Gallery());
                    return new TableGateway('gallery', $dbAdapter, null, $resultSetPrototype);
                },
                'Gallery\Model\GalleryImageTable' =>  function($sm) {
                    $tableGateway = $sm->get('GalleryImageTableGateway');
                    $table = new GalleryImageTable($tableGateway);
                    return $table;
                },
                'GalleryImageTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GalleryImage());
                    return new TableGateway('gallery_image', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}