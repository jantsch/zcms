<?php
/**
 * GalleryTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Gallery\Model;

use Login\Controller\LoginController;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

class GalleryTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getGalleryImages($gallery_id) {

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT * FROM gallery_image WHERE gallery_id=?";
        return $ad->query($sql, array($gallery_id));

    }

    public function removeGalleryImages($gallery_ids, $upload_folder)
    {

        if(!is_array($gallery_ids)) {
            $gallery_ids = array((int)$gallery_ids);
        }

        foreach($gallery_ids as &$g){
            $g = (int)$g;
        }
        unset($g);
        $in = implode(',', $gallery_ids);

        $ad = $this->tableGateway->getAdapter();
        $sql = "SELECT * FROM gallery_image WHERE gallery_id IN (?)";
        $resultSet = $ad->query($sql, array($in));
        $ids = array();
        $subs = scandir($upload_folder);

        foreach($resultSet as $row) {

            $ids[] = (int)$row->id;

            foreach($subs as $target_folder) {
                if( substr($target_folder,0,1)!='.') {
                    $target = $upload_folder . '/' . $target_folder . '/'  .$row->file;
                    if(is_file($target)) {
                        @unlink($target);
                    }
                }
            }
        }

        if(count($ids)>0) {
            $in = implode(',', $ids);
            $sql = "DELETE FROM gallery_image WHERE id IN (?)";
            $resultSet = $ad->query($sql, array($in));

        }

    }

    public function fetchPublic($page=1, $page_length=6)
    {
        $resultSet = $this->tableGateway->select(function($select) use($page,$page_length){
            $select->join('gallery_image', 'gallery.image_id = gallery_image.id', array('main_image'=>'file'), 'left');
            $select->order('created_on DESC');
            $select->limit(($page_length));
            $select->offset(($page-1)*$page_length);
            $select->where("status='public'");
        });
        return $resultSet;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select){
            $select->join('gallery_image', 'gallery.image_id = gallery_image.id', array('main_image'=>'file'), 'left');
            $select->order('created_on DESC');
        });
        return $resultSet;
    }

    public function getGallery($id)
    {
        $where = new Where();
        $single = true;
        if(is_array($id)) {
            $where->in('id', $id);
            $single = false;
        }else{
            $where->equalTo('id', $id);
        }

        $rowset = $this->tableGateway->select($where);

        $return = ($single ? $rowset->current() : $rowset);
        if (!$return) {
            throw new \Exception("Could not find row $id");
        }
        return $return;
    }

    public function saveGallery(Gallery $gallery)
    {
        $data = array(
            'title' => $gallery->title,
            'description'  => $gallery->description,
            'status'  => $gallery->status,
            'image_id'  => $gallery->image_id
        );

        $id = (int) $gallery->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = (int)LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            $this->tableGateway->getLastInsertValue();
            // jntx - postgres last insert val, must specify wich sequence
            return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('gallery_id_seq');

        } else {
            if ($this->getGallery($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = (int)LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Gallery id does not exist');
            }
        }
    }

    public function deleteGallery($id, $upload_folder)
    {
        $where = new Where();
        if(is_array($id)) {
            $where->in('id', $id);
        }else{
            $where->equalTo('id', $id);
        }

        $this->tableGateway->delete($where);

        $this->removeGalleryImages($id, $upload_folder);
    }
}