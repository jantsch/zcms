<?php
/**
 * GalleryImageTable.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Gallery\Model;

use Login\Controller\LoginController;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Form\Exception\ExtensionNotLoadedException;

class GalleryImageTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select(function($select){
            $select->order('order ASC');
        });
        return $resultSet;
    }

    public function fetchGallery($gallery_id)
    {

        $resultSet = $this->tableGateway->select(function($select) use ($gallery_id){
            $select->where->equalTo('gallery_id', $gallery_id);
            $select->order('order ASC');

        });

        return $resultSet;
    }

    public function getGalleryImage($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }


    public function saveGalleryImage(GalleryImage $image)
    {
        $data = array(
            'gallery_id' => $image->gallery_id,
            'title' => $image->title,
            'description' => $image->description,
            'file' => $image->file,
            'order' => $image->order
        );

        $id = (int) $image->id;
        if ($id == 0) {
            $data['created_on'] = date('Y-m-d H:i:s');
            $data['created_by'] = LoginController::loggedUser('id');
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();

        } else {
            if ($this->getGalleryImage($id)) {
                $data['modified_on']  = date('Y-m-d H:i:s');
                $data['modified_by']  = LoginController::loggedUser('id');
                $this->tableGateway->update($data, array('id' => $id));

            } else {
                throw new \Exception('Image id does not exist');
            }
        }

        return null;

    }

    public function deleteGalleryImage($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
}