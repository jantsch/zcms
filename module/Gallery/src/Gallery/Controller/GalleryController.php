<?php
/**
 * GalleryController.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

namespace Gallery\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Gallery\Form\GalleryForm;
use Gallery\Model\Gallery;
use Login\Controller\LoginController;

class GalleryController extends AbstractActionController
{

    protected $galleryTable;
    protected $galleryImgTbl;

    public function getGalleryTable()
    {
        if (!$this->galleryTable) {
            $sm = $this->getServiceLocator();
            $this->galleryTable = $sm->get('Gallery\Model\GalleryTable');
        }
        return $this->galleryTable;
    }

    /**
     *
     * @return \Gallery\Model\GalleryImageTable
     */
    public function getImgGalleryTable()
    {

        if (!$this->galleryImgTbl) {
            $sm = $this->getServiceLocator();
            $this->galleryImgTbl = $sm->get('Gallery\Model\GalleryImageTable');

        }
        return $this->galleryImgTbl;
    }

    public function indexAction()
    {

        LoginController::checkAuthentication();

         return new ViewModel(array(
            'galleries' => $this->getGalleryTable()->fetchAll(),
        ));
    }

    public function popupAction()
    {

        LoginController::checkAuthentication();

        $this->layout('layout/popup');
        return new ViewModel(array(
            'galleries' => $this->getGalleryTable()->fetchAll(),
        ));



    }

    public function getimagesAction()
    {

        LoginController::checkAuthentication();

        $id = (int)$this->params()->fromRoute('id', 0);
        $images = $this->getImgGalleryTable()->fetchGallery($id);
        $config = $this->getServiceLocator()->get('Config');

        $this->layout('layout/emtpy');
        return new ViewModel(array(
            'images' => $images,
            'img_folder' => $config['gallery_img_folder']
        ));


    }


    public function addAction()
    {
        LoginController::checkAuthentication();

        $form = new GalleryForm();
        $form->get('submit')->setValue('Salvar');

        $request = $this->getRequest();
        if ($request->isPost()) {

            $gallery = new Gallery();
            $gallery->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
            $form->setInputFilter($gallery->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $gallery->exchangeArray($form->getData());
                $id = (int)$this->getGalleryTable()->saveGallery($gallery, array('action'=>'list'));

                // Redirect to list of gallerys
                return $this->redirect()->toRoute('gallery', array('action' => 'edit', 'id'=> $id));
            }
        }
        return array('form' => $form);
    }

    public function editAction()
    {
        LoginController::checkAuthentication();

        // get's gallery id from route
        $id = (int)$this->params()->fromRoute('id', 0);

        if (!$id) {
            return $this->redirect()->toRoute('gallery', array(
                'action' => 'add'
            ));
        }

        // Get the Gallery with the specified id.  An exception is thrown
        // if it cannot be found, in which case go to the index page.
        try {
            $gallery = $this->getGalleryTable()->getGallery($id);
            $gallery->password = null;
            $gallery->setDbAdapter($this->getServiceLocator()->get('Zend\Db\Adapter\Adapter'));
        }
        catch (\Exception $ex) {
            return $this->redirect()->toRoute('gallery', array(
                'action' => 'index'
            ));
        }

        $form  = new GalleryForm();
        $form->bind($gallery);
        $form->get('submit')->setAttribute('value', 'Alterar');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($gallery->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getGalleryTable()->saveGallery($gallery);

                // Redirect to the list of gallerys
                return $this->redirect()->toRoute('gallery');
            }
        }

        $config = $this->getServiceLocator()->get('Config');

        return array(
            'id' => $id,
            'form' => $form,
            'images' => $this->getImgGalleryTable()->fetchGallery($id),
            'img_folder' => $config['gallery_img_folder']
        );
    }

    public function deleteAction()
    {
        LoginController::checkAuthentication();

        $request = $this->getRequest();
        $id = (int) $this->params()->fromRoute('id', 0);
        $gids = false;
        if (!$id) {

            $gids = $request->getPost('gid');

            if(is_array($gids) && count($gids)>0) {
                foreach($gids as &$g)
                    $g = (int)$g;
                unset($g);

            }else{
                // isn't a single nor a mass action
                return $this->redirect()->toRoute('gallery');
            }
        }else{
            $gids = array($id);
        }


        $del = $request->getPost('del', 'Cancelar');
        if ($request->isPost() && $del == 'Remover') {


            $cfg = $this->getServiceLocator()->get('Config');
            $this->getGalleryTable()->deleteGallery($gids, $cfg['gallery_upload_folder']);

            // Redirect to list
            return $this->redirect()->toRoute('gallery');
        }

        $names = $this->getGalleryTable()->getGallery($gids);

        return array(
            'gid'    => ($gids==false ? array($id) : $gids),
            'galleries' => $names
        );
    }

    public function uploadAction() {

        $request = $this->getRequest();

        // fix uploadify session (un)handling
        $session_name = session_name();
        if ($request->getPost($session_name, false)===false) {
            exit;
        } else {
            session_destroy();
            session_id($request->getPost($session_name, false));
            session_start();
        }

        LoginController::checkAuthentication();

        $this->layout('layout/json');

        $cfg = $this->getServiceLocator()->get('Config');
        $targetFolder = $cfg['gallery_upload_folder'];
        $json = array('success' => false);

        if (!empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = rtrim($targetFolder,'/') . '/';

            // Validate the file type
            $fileTypes = array('jpg','jpeg','gif','png');
            $fileParts = pathinfo($_FILES['Filedata']['name']);

            if (in_array(strotolower($fileParts['extension']),$fileTypes)) {

                // save and get the id
                $img = new \Gallery\Model\GalleryImage();
                $img->gallery_id = $request->getPost('gallery_id', 0);
                $img->title = '';
                $img->file = '';
                $id = $this->getImgGalleryTable()->saveGalleryImage($img);

                // get original file name... avoid colisions
                $mod = "";
                do {
                    $targetFileName = $id . ($mod . (empty($mod) ? "" : "_") ) . ".jpg";
                    $originalFile = $targetPath . 'original/' . $targetFileName;
                    $mod = (int)$mod+1;
                }while(file_exists($originalFile));

                // store folders
                $img->file = $targetFileName;
                $this->getImgGalleryTable()->saveGalleryImage($img);

                $resizedFile_800 = $targetPath . '800/' . $targetFileName;
                $resizedFile_780 = $targetPath . '780/' . $targetFileName;
                $resizedFile_88 = $targetPath . '88/' . $targetFileName;
                $resizedFile_278 = $targetPath . '278/' . $targetFileName;

                // resizes
                error_log("$tempFile, $resizedFile_800");
                \WideImage::load($tempFile)->resize(800, 600, 'inside')->saveToFile($resizedFile_800);
                \WideImage::load($tempFile)->resize(780, 373, 'outside')->crop('center', 'center', 780, 373)->saveToFile($resizedFile_780);
                \WideImage::load($tempFile)->resize(88, 88,'outside')->crop('center', 'center', 88, 88)->saveToFile($resizedFile_88);
                \WideImage::load($tempFile)->resize(278, 298,'outside')->crop('center', 'center', 278, 298)->saveToFile($resizedFile_278);
                move_uploaded_file($tempFile,$originalFile);

                $json = array('success' => true, 'src'=> $targetFileName, 'id' => $id);

            } else {

                $json =  array('success' => false, "msg" => 'Arquivo invalido.');
            }
        }

        return array('data'=> $json);

    }

    public function delimageAction() {

        LoginController::checkAuthentication();

        $this->layout('layout/json');

        $request = $this->getRequest();
        $id = (int)$request->getPost('id', 0);

        $data = array("msg" => "Imagem inválida.");

        if($id>0) {
            $image = $this->getImgGalleryTable()->getGalleryImage($id);
            $name = $id . ".jpg";

            $this->getImgGalleryTable()->deleteGalleryImage($id);

            $cfg = $this->getServiceLocator()->get('Config');
            $targetFolder = $cfg['gallery_upload_folder'];

            @unlink($targetFolder . 'original/' . $image->file);
            @unlink($targetFolder . '800/' . $name);
            @unlink($targetFolder . '780/' . $name);
            @unlink($targetFolder . '88/' . $name);

            $data = array("msg" => "Imagem removida.");
        }


        return array('data' => $data);

    }


}

