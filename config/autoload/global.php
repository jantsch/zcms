<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

$return = array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:host=localhost;port=3309;dbname=grgrupo',
        'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"),
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    'post_upload_folder' => APP_ROOT_PATH . "/public/uploads/post/",
    'post_img_folder' => "/uploads/post/",
    'post_img_sizes' => array(
        'original' => array('w' => null, 'h' => null, 'resize' => null, 'crop' => null),
        '92x72' => array('w' => 92, 'h' => 72, 'resize' => 'outside', 'crop' => 'center'),
        '278x298' => array('w' => 278, 'h' => 298, 'resize' => 'outside', 'crop' => 'center'),
        '796x381' => array('w' => 796, 'h' => 381, 'resize' => 'outside', 'crop' => 'center')
    ),
    'gallery_upload_folder' => APP_ROOT_PATH . "/public/uploads/gallery/",
    'gallery_img_folder' => "/uploads/gallery/",
    'banners_upload_folder' => APP_ROOT_PATH . "/public/uploads/banners/",
    'banners_img_folder' => "/uploads/banners/",
    'protype_upload_folder' => APP_ROOT_PATH . "/public/uploads/protype/",
    'protype_img_folder' => "/uploads/protype/",
    'product_upload_folder' => APP_ROOT_PATH . "/public/uploads/product/",
    'product_img_folder' => "/uploads/product/",
    'product_img_sizes' => array(
        'original' => array('w' => null, 'h' => null, 'resize' => null, 'crop' => null),
        '270x224' => array('w' => 270, 'h' => 224, 'resize' => 'outside', 'crop' => 'center'),
        '396x379' => array('w' => 396, 'h' => 379, 'resize' => 'outside', 'crop' => null),
        '125x80' => array('w' => 125, 'h' => 80, 'resize' => 'outside', 'crop' => 'center')
    ),
    'category_upload_folder' => APP_ROOT_PATH . "/public/uploads/category/",
    'category_img_folder' => "/uploads/category/",

);

if($_SERVER ["SERVER_NAME"]!= "zcms01.localhost") {
    $return['db'] = array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:host=grgrupolw.mysql.dbaas.com.br;dbname=grgrupolw',
        'driver_options' => array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'"),
    );

}

return $return;
