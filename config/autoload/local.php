<?php
/**
 * local.php.phtml
 * @author Gustavo Jantsch <jantsch@gmail.com>
 * @copyright Copyright (c) 2016 Gustavo Jantsch
 */

if($_SERVER ["SERVER_NAME"]== "localhost") {
    return array(
        'db' => array(
            'username' => 'root',
            'password' => '',
        ),
    );
}else{

    return array(
        'db' => array(
            'username' => '',
            'password' => '',
        ),
    );
}